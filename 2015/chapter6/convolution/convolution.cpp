#include <cv.h>
#include <highgui.h>
#include <iostream>
int main(int argc, char** argv)
{
	using namespace std;
	if(argc < 2)
	{
		cout<<"Input invalid"<<endl;
		return -1;
	}
	
	CvMat *kernel = cvCreateMat(3, 3, CV_8U);
	
	cvSetReal2D(kernel, 0, 0, 1);
	cvSetReal2D(kernel, 0, 1, -2);
	cvSetReal2D(kernel, 0, 2, 1);

	cvSetReal2D(kernel, 1, 0, 2);
	cvSetReal2D(kernel, 1, 1, -4);
	cvSetReal2D(kernel, 1, 2, 2);

	cvSetReal2D(kernel, 1, 0, 1);
	cvSetReal2D(kernel, 1, 1, -2);
	cvSetReal2D(kernel, 1, 2, 1);
	/*cvmSet(&kernel, 1, 2, -2.);
	cvmSet(&kernel, 1, 3, 1.);
	cvmSet(&kernel, 2, 1, 2.);
	cvmSet(&kernel, 2, 2, -4.);
	cvmSet(&kernel, 2, 3, 2.);
	cvmSet(&kernel, 3, 1, 1.);
	cvmSet(&kernel, 3, 2, -2.);
	cvmSet(&kernel, 3, 3, 1.);*/
	IplImage* image = cvLoadImage( argv[1],CV_LOAD_IMAGE_COLOR);
	IplImage* image2 = cvCreateImage( cvGetSize(image), 8, 3);
	IplImage* sig_image_r = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* sig_image_edge = cvCreateImage( cvGetSize(image), 8, 1);
	cvSplit( image, sig_image_r, NULL, NULL, NULL);
	cvCanny( sig_image_r, sig_image_edge, 10, 50);
	//cvFilter2D(image, image2, kernel);
	//cvLaplace(image, image2);
	cvNamedWindow("CV-Show",CV_WINDOW_NORMAL);
	
	cvShowImage("CV-Show",sig_image_edge);
	cvWaitKey(0);
	cvDestroyWindow("CV-Show");
	cvReleaseImage(&image);
}
