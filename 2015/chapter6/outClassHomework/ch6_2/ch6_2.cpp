#include "cv.h"
#include "highgui.h"
#include <iostream>
using namespace std;
int main(int argc, char** argv)
{

	if( argc < 2)
	{
		cout<<"Input parameters invalid"<<endl;
		return -1;
	}
	float kernel[] = { 1.f/16, 2.f/16, 1.f/16, 2.f/16, 4.f/16, 2.f/16, 1.f/16, 2.f/16, 1.f/16};
	CvMat Mkernel;
	Mkernel = cvMat( 3, 3, CV_32FC1, kernel);
	IplImage* image = cvLoadImage( argv[1] );
	IplImage* dst = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
	cvFilter2D( image, dst, &Mkernel, cvPoint(-1,-1));
	cvNamedWindow("2DFilter",CV_WINDOW_NORMAL);
	cvShowImage("2DFilter",dst);
	cvWaitKey(0);

	cvReleaseImage(&image);
	cvReleaseImage(&dst);

	cvDestroyAllWindows;
}
