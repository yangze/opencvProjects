#include "cv.h"
#include "highgui.h"
#include <iostream>
using namespace std;
int main(int argc, char** argv)
{

	if( argc < 2)
	{
		cout<<"Input parameters invalid"<<endl;
		return -1;
	}
	/*float kernel[] = { 1.f/4,2.f/4,1.f/4,0,0,0,0,0,0 };
	float kernel1[] ={ 1.f/4,0,0,2.f/4,0,0,1.f/4,0,0 };
	CvMat Mkernel;
	CvMat Mkernel1;
	Mkernel = cvMat( 3, 3, CV_32F, kernel);
	Mkernel1 = cvMat( 3, 3, CV_32F, kernel1);*/

	
	IplImage* image = cvLoadImage( argv[1] ,CV_LOAD_IMAGE_GRAYSCALE);
	IplImage* dst = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
	
	IplImage* dst1 = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
	/*cvFilter2D( image, dst, &Mkernel, cvPoint(-1,-1));

	cvFilter2D( dst, dst1, &Mkernel1, cvPoint(-1,-1));*/

	cvCanny( image, dst, 10, 50, 3);
	cvNamedWindow("2DFilter",CV_WINDOW_NORMAL);
	
	cvShowImage("2DFilter",dst);

	cvWaitKey(0);

	cvReleaseImage(&image);
	cvReleaseImage(&dst);
	cvReleaseImage(&dst1);
	cvDestroyAllWindows;
}
