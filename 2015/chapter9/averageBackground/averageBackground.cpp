#include "cv.h"
#include "highgui.h"

void AllocateImages(IplImage* I);
void accmulateBackground(IplImage *I);
void createModelsfromStats();
void setHighThreshold(float scale);
void setLowThreshold(float scale);
void backgroundDiff(IplImage *I, IplImage *Imask);
//global storage
//
//Float,3-channels images
//
IplImage *IavgF,*IdiffF,*IhiF,*IlowF,*IprevF;
IplImage *Iscratch,*Iscratch2;

//Float,1-channel images
IplImage *Igray1,*Igray2,*Igray3;
IplImage *Ilow1,*Ilow2,*Ilow3;
IplImage *Ihi1,*Ihi2,*Ihi3;

//Byte,1-channel image
IplImage *Imaskt;

//counts number of image learned for averaging later

float Icount;

//I is just a sample image for allocation purpose

void AllocateImages(IplImage* I)
{
	CvSize sz = cvGetSize(I);

	IavgF = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	IdiffF = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	IhiF = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	IlowF = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	IprevF = cvCreateImage( sz, IPL_DEPTH_32F, 3);

	Ilow1 = cvCreateImage( sz, IPL_DEPTH_32F, 1);
	Ilow2 = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Ilow3 = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Ihi1 = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Ihi2 = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Ihi3 = cvCreateImage( sz, IPL_DEPTH_32F, 3);

	cvZero(IavgF);
	cvZero(IdiffF);
	cvZero(IprevF);
	cvZero(IhiF);
	cvZero(IlowF);

	Icount = 0.0001;	//Protect against divide by zero
	Iscratch = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Iscratch2 = cvCreateImage( sz, IPL_DEPTH_32F, 3);
	Igray1 = cvCreateImage( sz, IPL_DEPTH_32F, 1);
	Igray2 = cvCreateImage( sz, IPL_DEPTH_32F, 1);
	Igray3 = cvCreateImage( sz, IPL_DEPTH_32F, 1);
	Imaskt = cvCreateImage( sz, IPL_DEPTH_8U, 1);
	cvZero(Iscratch);
	cvZero(Iscratch2);
}
//Learn the background statistic for one more frame
//I is a color sample of the background,3-channel,8u
void accmulateBackground(IplImage *I)
{
	static int first = 1;
	cvCvtScale(I,Iscratch,1,0);//convert to float
	if(!first)
	{
		cvAcc(Iscratch, IavgF);
		cvAbsDiff(Iscratch, IprevF, Iscratch2);
		cvAcc(Iscratch2,IdiffF);
		Icount +=1.0;
	}
	first = 0;
	cvCopy(Iscratch, IprevF);
}

void createModelsfromStats()
{
	cvConvertScale( IavgF, IavgF, (double)(1.0/Icount));
	cvConvertScale( IdiffF, IdiffF, (double)(1.0/Icount));
	//make sure diff is always something
	cvAddS( IdiffF, cvScalar( 1.0, 1.0, 1.0), IdiffF );
	setHighThreshold(7.0);
	setLowThreshold(6.0);
}
void setHighThreshold(float scale)
{
	cvConvertScale( IdiffF, Iscratch, scale );
	cvAdd( Iscratch, IavgF, IhiF);
	cvSplit( IhiF, Ihi1, Ihi2, Ihi3, NULL);
}

void setLowThreshold(float scale)
{
	cvConvertScale( IdiffF, Iscratch, scale );
	cvSub( IavgF, Iscratch, IlowF);
	cvSplit( IlowF, Ilow1, Ilow2, Ilow3, NULL);
}
//Create a binary:0,255 mask where 255 means foreground pixel
//I Input image,3-channels,8u
//Imask Mask image to be created,1-channel 8u
//
void backgroundDiff(IplImage *I, IplImage *Imask)
{
	cvCvtScale( I, Iscratch, 1, 0);//to flolat
	cvSplit( Iscratch, Igray1, Igray2, Igray3, NULL);

	//channel 1
	//
	cvInRange( Igray1, Ilow1, Ihi1, Imask );
	//channel 2
	//
	cvInRange( Igray2, Ilow2, Ihi2, Imask );
	cvOr(Imask, Imask, Imask);
	//channel 3
	//
	cvInRange( Igray3, Ilow3, Ihi3, Imask );
	cvOr(Imask, Imask, Imask);
	//Finally invert the results
	//
	cvSubRS( Imask, cvScalar(255), Imask );
}
// clear memory
void DeallocateImages()
{
	cvReleaseImage( &IavgF );
	cvReleaseImage( &IdiffF );
	cvReleaseImage( &IprevF );
	cvReleaseImage( &IhiF );
	cvReleaseImage( &IlowF );
	cvReleaseImage( &Ilow1 );
	cvReleaseImage( &Ilow2 );
	cvReleaseImage( &Ilow3 );
	cvReleaseImage( &Ihi1 );
	cvReleaseImage( &Ihi2 );
	cvReleaseImage( &Ihi3 );
	cvReleaseImage( &Iscratch );
	cvReleaseImage( &Iscratch2 );
	cvReleaseImage( &Igray1 );
	cvReleaseImage( &Igray2 );
	cvReleaseImage( &Igray3 );
	cvReleaseImage( &Imaskt );
}

int main( int argc, char** argv)
{
	
}
