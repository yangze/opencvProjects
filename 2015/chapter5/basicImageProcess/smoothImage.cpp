#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <time.h>


int main(int argc, char**argv)
{
	using namespace std;
	if( argc < 2)
	{
		return -1;
	}

	IplImage* image = NULL;
	IplImage* tempImage = NULL;
	IplImage* tempImage2 = NULL;
	IplImage* resultImage = NULL;
	image = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if( !image )
	{
		cout<<"Load error@@"<<endl;
		return -1;
	}
	IplImage* r = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* g = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* b = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* sig = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* sig2 = cvCreateImage( cvGetSize(image), 8, 1);
	// assign every data to each channel
	cvSplit( image, r, g, b, NULL);
	
	tempImage = cvCreateImage(cvGetSize(image), 8, 3);
	tempImage2 = cvCreateImage(cvGetSize(image), 8, 3 );
	resultImage = cvCreateImage(cvGetSize(image), 8, 3);
	//cvFlip
	//cvFlip( image, tempImage, -1);
	//cvSmooth-CV_MEDIAN this method support in place operation 
	//cvSmooth( image, tempImage, CV_MEDIAN, 3, 3);
	//cvSmooth-CV_GAUSSIAN this method support in place operation
	//cvSmooth( image, image, CV_GAUSSIAN, 3, 3);
	//cvSmooth-CV_BILATERAL
	//cvSmooth( image, tempImage, CV_BILATERAL, 10, 10);
	//cvErode(image, tempImage, NULL, 1);
	//cvDilate( image, tempImage, NULL, 1);
	//cvErode(image, tempImage2, NULL, 1);
	//cvMorphologyEx(image, tempImage, NULL, NULL, CV_MOP_OPEN);
	///cvAbsDiff(image, tempImage, resultImage);

	cvThreshold( r, sig, 150, 255, CV_THRESH_BINARY);
	cvAdaptiveThreshold (r, sig2, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 3, 5);
	cvNamedWindow("Fixed threshold", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Adaptive threshold", CV_WINDOW_AUTOSIZE);
	cvShowImage("Fixed threshold", sig);
	cvShowImage("Adaptive threshold", sig2);
	cvWaitKey(0);
	cvReleaseImage(&tempImage);
	cvReleaseImage(&tempImage2);
	cvReleaseImage(&r);
	cvReleaseImage(&g);
	cvReleaseImage(&b);
	cvReleaseImage(&sig);
	cvReleaseImage(&sig2);
	cvReleaseImage(&image);
	
	cvDestroyWindow("Fixed threshold");
}
