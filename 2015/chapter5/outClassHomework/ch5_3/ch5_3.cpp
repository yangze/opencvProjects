#include <cv.h>
#include <highgui.h>
#include <iostream>

int main( int argc , char** argv )
{
	using namespace std;
	if( argc < 2)
	{
		cout<<"Input arguments invalid"<<endl;
		return -1;
	}

	IplImage* image = cvLoadImage( argv[1] );
	if( !image )
	{
		cout<<"Load operation failed"<<endl;
		return -1;
	} 
	IplImage* image2 = cvCreateImage( cvGetSize(image), 8, 3);
	IplImage* image3 = cvCreateImage( cvGetSize(image), 8, 3);
	cvNamedWindow("Image-Win", CV_WINDOW_NORMAL);
	cvNamedWindow("Image-Win2",CV_WINDOW_NORMAL);
	cvSmooth( image, image2, CV_GAUSSIAN, 0, 0, 0, 0);
	//cvSmooth( image2, image3, CV_GAUSSIAN, 0, 0, 1, 1);
	cvShowImage("Image-Win", image2 );
	cvShowImage("Image-Win2", image3 );
	cvWaitKey(0);
	cvReleaseImage(&image);
	cvReleaseImage(&image2);
	cvReleaseImage(&image3);
	cvDestroyWindow("Image-Win");
	cvDestroyWindow("Image-Win2");
}
