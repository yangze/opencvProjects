#include <cv.h>
#include <highgui.h>
#include <iostream>

int main(int argc, char** argv)
{
	using namespace std;
	/*if( argc < 2)
	{
		cout<<"Input arguments is invalid"<<endl;
		return -1;
	}*/
	IplImage* image = cvCreateImage(cvSize(100,100), 8, 1);
	
	if( !image)
	{
		cout<<"Create Image failed"<<endl;
		return -1;
	}
	
	
	cvNamedWindow("Image-Win",CV_WINDOW_NORMAL);
	cvZero(image);
	*(image->imageData + 50*image->widthStep + 50) = 255;
	*(image->imageData + 51*image->widthStep + 50) = 255;
	for(int i = 0; i < 2; i++)
	{
		cvSmooth(image, image, CV_GAUSSIAN, 3, 3);
	}
	cvShowImage("Image-Win", image);

	cvWaitKey(0);
	cvReleaseImage(&image);
	cvDestroyWindow("Image-Win");
}
