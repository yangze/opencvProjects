#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <time.h>


int main(int argc, char**argv)
{
	using namespace std;
	if( argc < 2)
	{
		return -1;
	}

	IplImage* image = NULL;
	IplImage* tempImage = NULL;
	IplImage* tempImage2 = NULL;
	IplImage* tempImage3 = NULL;
	IplImage* resultImage = NULL;
	image = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if( !image )
	{
		cout<<"Load error@@"<<endl;
		return -1;
	}
	IplImage* r = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* g = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* b = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* sig = cvCreateImage( cvGetSize(image), 8, 1);
	IplImage* sig2 = cvCreateImage( cvGetSize(image), 8, 1);
	// assign every data to each channel
	cvSplit( image, r, g, b, NULL);
	
	tempImage = cvCreateImage(cvGetSize(image), 8, 3);
	tempImage2 = cvCreateImage(cvGetSize(image), 8, 3 );
	tempImage3 = cvCreateImage(cvGetSize(image), 8, 3);
	//cvFlip
	//cvFlip( image, tempImage, -1);
	//cvSmooth-CV_MEDIAN this method support in place operation 
	//cvSmooth( image, tempImage, CV_MEDIAN, 3, 3);
	//cvSmooth-CV_GAUSSIAN this method support in place operation
	//cvSmooth( image, image, CV_GAUSSIAN, 3, 3);
	//cvSmooth-CV_BILATERAL
	//cvSmooth( image, tempImage, CV_BILATERAL, 10, 10);
	//cvErode(image, tempImage, NULL, 1);
	//cvDilate( image, tempImage, NULL, 1);
	//cvErode(image, tempImage2, NULL, 1);
	//cvMorphologyEx(image, tempImage, NULL, NULL, CV_MOP_OPEN);
	///cvAbsDiff(image, tempImage, resultImage);

	/*cvThreshold( r, sig, 150, 255, CV_THRESH_BINARY);
	cvAdaptiveThreshold (r, sig2, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 3, 5);*/

	cvSmooth( image, tempImage, CV_GAUSSIAN, 3, 3);
	cvSmooth( image, tempImage2, CV_GAUSSIAN, 5, 5);
	cvSmooth( tempImage2, tempImage2, CV_GAUSSIAN, 5, 5);
	cvSmooth( image, tempImage3, CV_GAUSSIAN, 11, 11);
	cvNamedWindow("3X3", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("5X5", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("9X9", CV_WINDOW_AUTOSIZE);
	cvShowImage("3X3", image);
	cvShowImage("5X5", tempImage2);
	cvShowImage("9X9", tempImage3);
	cvWaitKey(0);
	cvReleaseImage(&tempImage);
	cvReleaseImage(&tempImage2);
	cvReleaseImage(&tempImage3);
	cvReleaseImage(&r);
	cvReleaseImage(&g);
	cvReleaseImage(&b);
	cvReleaseImage(&sig);
	cvReleaseImage(&sig2);
	cvReleaseImage(&image);
	
	cvDestroyWindow("3X3");
	cvDestroyWindow("5X5");
	cvDestroyWindow("9X9");
}
