#include <cv.h>
#include <highgui.h>
#include <iostream>
//
int max_iters = 3;
int open_close_pos = 0;
int erode_dilate_pos = 0;
int element_shape = CV_SHAPE_RECT;
IplConvKernel* element = NULL;
unsigned char* target_pos = NULL;
int g_find_flag = 0;
uchar* ptr = NULL;
int main( int argc , char** argv )
{
	using namespace std;
	int coor_row = 0;
	int coor_cols = 0;
	if( argc < 3)
	{
		cout<<"Input arguments invalid"<<endl;
		return -1;
	}

	IplImage* image1 = cvLoadImage( argv[1] );
	IplImage* image2 = cvLoadImage( argv[2] );
	if( !image1 || !image2)
	{
		cout<<"Load operation failed"<<endl;
		return -1;
	} 
	IplImage* temp1 = cvCreateImage( cvGetSize(image1), 8, 3);
	IplImage* temp2 = cvCreateImage( cvGetSize(image2), 8, 3);
	IplImage* temp3 = cvCreateImage( cvGetSize(image2), 8, 3);
	IplImage* r = cvCreateImage( cvGetSize(image2), 8, 1);
	IplImage* g = cvCreateImage( cvGetSize(image2), 8, 1);
	IplImage* b = cvCreateImage( cvGetSize(image2), 8, 1);

	//cvNamedWindow("Image-Win1", CV_WINDOW_NORMAL);
	cvNamedWindow("Image-Win2",CV_WINDOW_NORMAL);
	//cvNamedWindow("Image-Win3",CV_WINDOW_NORMAL);
	cvAbsDiff( image1, image2, temp1);

	cvSplit( temp1, r, g, b, NULL);
	//二值化 图像
	cvThreshold(r, r, 50, 255, CV_THRESH_BINARY);

	int n = open_close_pos - max_iters;
	int an = n > 0? n:-n;
	element = cvCreateStructuringElementEx(an*2+1, an*2+1, an, an, element_shape, 0);
	//形态学操作 Morphology Excution - Open Operation
	cvMorphologyEx( r, r, NULL, element, CV_MOP_OPEN);
	int  row,cols;
	//遍历图像
	for( row = 0; row < r->height; row++ )
	{
		ptr = (uchar*)(r->imageData + row*r->widthStep);
		
		for(  cols = 0; cols < r->width; cols ++ )
		{
			
			if(*ptr++ == 255)
			{
				target_pos = ptr;
				coor_row = row;
				coor_cols = cols;
				g_find_flag = 1;
				break;
			}
		}
		if( g_find_flag )
			break;
	}
	CvConnectedComp comp;
	
	cvFloodFill(r, cvPoint(coor_row, coor_cols), cvScalar(100), cvScalarAll(0), cvScalarAll(0), &comp);

	
	
	//CvSeq* seq = comp.contour;
	//cout<<seq->elem_size<<endl;
	//cout<<"area"<<seq->total<<endl;
	/*for( int i = 0; i < seq->total; i++)
	{
		
		CvConnectedComp* tmp = (CvConnectedComp*) cvGetSeqElem(seq,i);
	}*/
	cvShowImage( "Image-Win2", r );
	//cvShowImage( "Image-Win3", temp1 );
	cvWaitKey(0);
	cvReleaseImage(&image1);
	cvReleaseImage(&image2);
	cvReleaseImage(&temp1);
	cvReleaseImage(&temp2);
	//cvDestroyWindow("Image-Win1");
	cvDestroyWindow("Image-Win2");
	//cvDestroyWindow("Image-Win3");
}
