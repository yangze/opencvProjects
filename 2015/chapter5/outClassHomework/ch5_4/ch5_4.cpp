#include <cv.h>
#include <highgui.h>
#include <iostream>

int main( int argc , char** argv )
{
	using namespace std;
	if( argc < 3)
	{
		cout<<"Input arguments invalid"<<endl;
		return -1;
	}

	IplImage* image1 = cvLoadImage( argv[1] );
	IplImage* image2 = cvLoadImage( argv[2] );
	if( !image1 || !image2)
	{
		cout<<"Load operation failed"<<endl;
		return -1;
	} 
	IplImage* temp1 = cvCreateImage( cvGetSize(image1), 8, 3);
	IplImage* temp2 = cvCreateImage( cvGetSize(image2), 8, 3);
	IplImage* temp3 = cvCreateImage( cvGetSize(image2), 8, 3);
	cvNamedWindow("Image-Win1", CV_WINDOW_NORMAL);
	cvNamedWindow("Image-Win2",CV_WINDOW_NORMAL);
	cvNamedWindow("Image-Win3",CV_WINDOW_NORMAL);
	cvAbsDiff( image1, image2, temp1);
	
	cvErode(temp1, temp2);
	cvDilate(temp2,temp2);

	cvDilate(temp1,temp3);
	cvErode(temp3,temp3);
	
	//cvSmooth( image2, image3, CV_GAUSSIAN, 0, 0, 1, 1);
	cvShowImage( "Image-Win1", temp1 );
	cvShowImage( "Image-Win2", temp2 );
	cvShowImage( "Image-Win3", temp3 );
	cvWaitKey(0);
	cvReleaseImage(&image1);
	cvReleaseImage(&image2);
	cvReleaseImage(&temp1);
	cvReleaseImage(&temp2);
	cvDestroyWindow("Image-Win1");
	cvDestroyWindow("Image-Win2");
	cvDestroyWindow("Image-Win3");
}
