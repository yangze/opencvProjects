#include "opencv2/highgui/highgui.hpp"

int main( int argc, char** argv )
{
	cv::Mat img = cv::imread(argv[1],-1);
	if( img.empty() ) return -1;
	cv::namedWindow("Load-Image",cv::WINDOW_AUTOSIZE);
	cv::imshow("Load-Image",img);
	cv::waitKey(0);
	cv::destroyWindow("Load-Image");
}
