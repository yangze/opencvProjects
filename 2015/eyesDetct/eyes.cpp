
#include "time.h"
#include "cv.h"
#include "highgui.h"
using namespace std;
CvRect eye_area = cvRect(-1,-1,0,0);
CvRect current_eye_area;
int flag = 0;

CvHaarClassifierCascade* load_object_detector( const char* cascade_path )
{
	return (CvHaarClassifierCascade*)cvLoad( cascade_path );
}
CvHaarClassifierCascade* cascade = load_object_detector("haarcascade_righteye_2splits.xml");

int detect_and_draw_objects0000( IplImage* image,
								CvHaarClassifierCascade* cascade,
								int do_pyramids )
{
	IplImage* small_image;
	if(image) small_image = image;
	else return(0);
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* faces;
	int i, scale = 1;
	CvSize r = cvSize(15 , 15);
	int flag = 0;

	/* if the flag is specified, down-scale the 输入图像 to get a
	performance boost w/o loosing quality (perhaps) */
	if( do_pyramids )
	{
		small_image = cvCreateImage( cvSize(image->width/2,image->height/2), 
			IPL_DEPTH_8U, 3 );
		cvPyrDown( image, small_image, CV_GAUSSIAN_5x5 );
		scale = 2;
	}

	/* use the fastest variant */
	faces = cvHaarDetectObjects( small_image, cascade, storage, 1.01,4,CV_HAAR_DO_CANNY_PRUNING,r
		);

	/* draw all the rectangles */
	flag = faces->total;
	for( i = 0; i < faces->total; i++ )
	{
		/* extract the rectanlges only */
		CvRect face_rect = *(CvRect*)cvGetSeqElem( faces, i );
		cvRectangle( image, cvPoint(face_rect.x*scale,face_rect.y*scale),
			cvPoint((face_rect.x+face_rect.width)*scale,
			(face_rect.y+face_rect.height)*scale),
			CV_RGB(255,0,0), 3 );
		//if(eye_area.x == -1 || face_rect.x*scale > eye_area.x)
		eye_area = cvRect(face_rect.x*scale,face_rect.y*scale,face_rect.width*scale,face_rect.height*scale);
	}

	if( small_image != image )
		cvReleaseImage( &small_image );
	cvReleaseMemStorage( &storage );
	return(flag);
}


int detect_and_draw_objects( IplImage* image,IplImage* source_image,CvRect rr,
							CvHaarClassifierCascade* cascade,
							int do_pyramids )
{
	IplImage* small_image = image;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* faces;
	int i, scale = 1;
	CvSize r = cvSize(5 , 5);
	int flag = 0;

	/* if the flag is specified, down-scale the 输入图像 to get a
	performance boost w/o loosing quality (perhaps) */
	if( do_pyramids )
	{
		small_image = cvCreateImage( cvSize(image->width/2,image->height/2), 
			IPL_DEPTH_8U, 3 );
		cvPyrDown( image, small_image, CV_GAUSSIAN_5x5 );
		scale = 2;
	}

	/* use the fastest variant */
	faces = cvHaarDetectObjects( small_image, cascade, storage, 1.05,3,CV_HAAR_DO_CANNY_PRUNING,r
		);

	/* draw all the rectangles */
	flag = faces->total;
	printf("%d\n",flag);
	for( i = 0; i < faces->total; i++ )
	{
		/* extract the rectanlges only */
		CvRect face_rect = *(CvRect*)cvGetSeqElem( faces, i );
		current_eye_area = cvRect(rr.x+face_rect.x*scale,rr.y+face_rect.y*scale,face_rect.width*scale,face_rect.height*scale);
		cvRectangle( source_image, cvPoint(rr.x + face_rect.x*scale,rr.y+face_rect.y*scale),
			cvPoint(rr.x+(face_rect.x+face_rect.width)*scale,
			rr.y+(face_rect.y+face_rect.height)*scale),
			CV_RGB(255,0,0), 3 );

	}

	if( small_image != image )
		cvReleaseImage( &small_image );
	cvReleaseMemStorage( &storage );
	return(flag);
}


int harr(IplImage* image , CvRect r , IplImage* source_image)        //harr检测；若image中有汽车，返回1，否则返回0
{
	int flag;
	flag = 0;
	flag = detect_and_draw_objects( image, source_image ,r ,cascade, 1);
	return (flag);
}

int lharr(CvRect r , IplImage*image)          //进行harr检测的处理模块
{
	int width , height , flag;
	CvSize rr; 
	IplImage* image1;
	width = r.width; 
	height = r.height;
	rr = cvSize(width , height);
	image1 = cvCreateImage( rr, 8, 3);        //创建与ROI等大的图片

	cvSetImageROI(image,r);
	cvCopy(image,image1); 
	cvResetImageROI(image);
	flag = harr(image1 ,r , image);
	cvReleaseImage(&image1);
	return(flag);
}

void equalizehist(IplImage * image)               //直方图均衡归一化图像亮度并增强对比度
{
	int i;
	IplImage *pImageChannel[4] = { 0, 0, 0, 0 };
	IplImage *pSrcImage = image; 
	IplImage *pImage = cvCreateImage(cvGetSize(pSrcImage), pSrcImage->depth, pSrcImage->nChannels);
	if( pSrcImage )
	{
		for( i = 0; i < pSrcImage->nChannels; i++ )
		{
			pImageChannel[i] = cvCreateImage( cvGetSize(pSrcImage), pSrcImage->depth, 1 );
		}
		// 信道分离
		cvSplit( image, pImageChannel[0], pImageChannel[1],
			pImageChannel[2], pImageChannel[3] );
		for( i = 0; i < pImage->nChannels; i++ )
		{
			// 直方图均衡化
			cvEqualizeHist( pImageChannel[i], pImageChannel[i] );
		}
		// 信道组合
		cvMerge( pImageChannel[0], pImageChannel[1], pImageChannel[2],
			pImageChannel[3], image );
		// 释放资源
		for( i = 0; i < pSrcImage->nChannels; i++ )
		{
			if ( pImageChannel[i] )
			{
				cvReleaseImage( &pImageChannel[i] );
				pImageChannel[i] = 0;
			}
		}
		cvReleaseImage( &pImage );
		pImage = 0;
	}
}

int ostu(IplImage *dst_gray)
{
	IplImage * image;
	image=dst_gray;
	int w = image->width;
	int h = image->height;

	unsigned char *np;               // 图像指针
	unsigned char pixel;
	int thresholdValue=1;            // 阈值
	int ihist[256];                  // 图像直方图，256个点

	int i, j, k;                     
	int n, n1, n2, gmin, gmax;
	double m1, m2, sum, csum, fmax, sb;


	memset(ihist, 0, sizeof(ihist));// 对直方图置零

	gmin=255; gmax=0;

	for (i = 0; i < h; i++)         // 生成直方图
	{
		np = (unsigned char*)(image->imageData + image->widthStep*i);
		for (j = 0; j < w; j++) 
		{
			pixel = np[j];
			ihist[ pixel]++;
			if(pixel > gmax) gmax= pixel;
			if(pixel < gmin) gmin= pixel;
		}
	}

	// 设置
	sum = csum = 0.0;
	n = 0;

	for (k = 0; k <= 255; k++) 
	{
		sum += k * ihist[k];          // 质量矩
		n += ihist[k];                // 质量 
	}

	if (!n) 
	{
		return(20);                 //得到的阈值小于20时，设置为20并返回
	}

	//otsu全局阈值计算
	fmax = -1.0;
	n1 = 0;
	for (k = 0; k < 255; k++) 
	{
		n1 += ihist[k];
		if (!n1) { continue; }
		n2 = n - n1;
		if (n2 == 0) { break; }
		csum += k *ihist[k];
		m1 = csum / n1;               // 当请总质量矩/当前像素点数
		m2 = (sum - csum) / n2;       // 其后总质量
		sb = n1 * n2 *(m1 - m2) * (m1 - m2);
		// bbg: note: can be optimized.
		if (sb > fmax)
		{
			fmax = sb;
			thresholdValue = k;
		}
	}
	return(thresholdValue);
}




int main()
{
	IplImage* image;
	IplImage* pyr;
	IplImage* eye_image_grey;
	CvMemStorage *stor;                      //内存存储器,可用来存储诸如序列，轮廓，图形,子划分等动态增长数据结构的底层结构
	CvSeq *cont; 
	int start,end;
	int time = 0;
	int times = 0;
	int now_flag = 0;
	int pre_flag = 0;
	int x,y;
	int L = 0,M = 0;
	int plevel;
	int PLEVEL;
	int threshold;
	char c = '0'; 
	FILE* f;
	FILE* f1;
	f1 = fopen("eyetime.txt" , "w+t");
	fprintf(f1 , "%d" , 0);
	fclose(f1);

	f = fopen("camera.txt" , "w+t");
	fprintf(f , "%d" , 1);
	fclose(f);
	cvNamedWindow( "1", 0 );
	cvNamedWindow( "eye_area", 0 );

	cvResizeWindow("1",320,240); 
	cvMoveWindow("1", 800, 150); 
	IplImage* img = 0;
	while(c != '1')
	{
		f = fopen("camera.txt" , "r+t");
		fscanf(f , "%c" , &c);
		fclose(f);
	}
	CvCapture* capture = cvCaptureFromCAM(0);

	if(!cvQueryFrame(capture))
	{              // capture a frame 
		printf("Could not grab a frame\n");
		exit(0);
	}
	image = cvQueryFrame(capture);
	/*
	do
	{
		image = cvQueryFrame(capture);
		while(!detect_and_draw_objects0000( image, cascade, 1 ))
		{
			image = cvQueryFrame(capture);
		}
		cvShowImage( "1", image );
		cvWaitKey(2);
	}while(cvWaitKey(0) != 121);*/
	int scalex = 2;
	int scaley = 2.5;
	x = eye_area.x+eye_area.width/2;
	y = eye_area.y+eye_area.height/2;
	eye_area.x = x - scalex*eye_area.width/2;
	if(eye_area.x < 0) eye_area.x = 0;
	eye_area.y = y - scaley*eye_area.height/2;
	if(eye_area.y < 0) eye_area.y = 0;
	x = x + scalex*eye_area.width/2;
	y = y + scaley*eye_area.height/2;
	if(x > 640) x = 640;
	if(y > 480) y = 480;
	eye_area.width = 640;
	eye_area.height = 320;

	int time1 = 0 , flag = 1;
	while(cvWaitKey(2) == -1)
	{
		time1++;
		if(time1%3 == 0)
		{
			f = fopen("camera.txt" , "r+t");
			if(f)
			{
				fscanf(f , "%c" , &c);
				if(c == '1') 
				{
					if(flag == 0) 
					{
						printf("Open USB camera \n");
						capture = cvCaptureFromCAM(0);
						flag = 1;
					}

				}
				else 
				{
					if(flag == 1)
					{
						printf("Close USB camera \n");
						cvReleaseCapture(&capture);
						image = NULL;
						flag = 0;
					}

				}
				fclose(f);
			}
			else printf("Unknow peoblems \n");
		}
		if(flag == 1)                 
		{
			image=cvQueryFrame(capture);
			if(image)
			{
				start=clock();
				
				cvSetImageROI(image , eye_area);
				
				equalizehist(image);
				if(lharr(eye_area,image) <= 0) {now_flag = 0;printf("Can't find object ");}
				else 
				{
					now_flag = 1;printf("Find object ");
					int width , height , flag;
					CvSize rr; 
					IplImage* eye_image;
					IplImage* eye_image_grey;
					width = current_eye_area.width; 
					height = current_eye_area.height;
					rr = cvSize(width , height);
					eye_image = cvCreateImage( rr, 8, 3);        //创建与ROI等大的图片
					eye_image_grey = cvCreateImage(rr, 8 , 1);

					cvResetImageROI(image);
					cvSetImageROI(image,current_eye_area);
					cvCopy(image,eye_image); 
					cvResetImageROI(image);
					cvSetImageROI(image,eye_area);
                 
					cvCvtColor(eye_image, eye_image_grey , CV_BGR2GRAY);
					threshold = ostu(eye_image_grey) - 10;
					printf("ostu:%d  " , threshold);
					cvThreshold(eye_image_grey, eye_image_grey, threshold, 255.0, CV_THRESH_BINARY);
 
					int i , j;
					int flag1 = 0;
					unsigned char *np;               // 图像指针
					L = 0;
					for (i = height/2; i < height; i++)        
					{
						np = (unsigned char*)(eye_image_grey->imageData + eye_image_grey->widthStep*i);
						for (j = 0; j < width; j++) 
						{
							if(np[j] == 0) L = L+1;

						}
						
					}
					printf("L = %d" , L);
					if(time1 < 20) M = M+L;
					else if(time1 == 20) 
					{
						M = M/20;
						printf("M = %d" , M);
						cvWaitKey(0);
					}
					else
					{
						if(L < M*0.2)  
						{
							plevel = 1;  printf("Very tired ");
						}
						else if(L < M*0.5) 
						{
							plevel = 0.6; printf("Tired ");
						}
						else if(L < M*0.8) 
						{
							plevel = 0.3; printf("Pretty tired ");
						}
					}
					cvRectangle(eye_image_grey, cvPoint(0,height/2),
			        cvPoint(width,height),CV_RGB(255,0,0),3);
				


					cvShowImage("eye_area" , eye_image_grey);
					cvWaitKey(2);
					cvReleaseImage(&eye_image);
					cvReleaseImage(&eye_image_grey);


				}
				if(pre_flag != now_flag)  times++;
				pre_flag = now_flag;
				++time;
				if(time%600 == 0) 
				{
					printf("Sum of wink %d\n",times/2);
					f1 = fopen("eyetime.txt" , "w+t");
					fprintf(f1 , "%d" , times/2);
					times = 0;
					fclose(f1);
				}
				end = clock();
				//printf("Cost time %d\n" , end - start);
				cvShowImage( "1", image );
				cvWaitKey(2);
			}
		}
		else 
		{
			if(image) 
				cvShowImage( "1", image );
			cvWaitKey(2);
		}
	}


	cvReleaseHaarClassifierCascade( &cascade);
	cvReleaseImage( &image );
	cvReleaseCapture(&capture);
	return (0);
}

