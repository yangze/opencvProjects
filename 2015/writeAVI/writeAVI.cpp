#include <opencv2/opencv.hpp>
#include <iostream>

int main(int argc, char** argv)
{
	cv::namedWindow("Cam-Original",cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Cam-Canny",cv::WINDOW_AUTOSIZE);
	cv::VideoCapture cap;
	cap.set( CV_CAP_PROP_FRAME_WIDTH,320 );
	cap.set( CV_CAP_PROP_FRAME_HEIGHT,240 );
	double fps = cap.get(CV_CAP_PROP_FPS);
	
	cv::Size size(640, 
		      480);
	cv::VideoWriter writer;
	//std::cerr<<"WIDTH is"<<(int)cap.get(CV_CAP_PROP_FRAME_WIDTH)<<std::endl;
	//std::cerr<<"HEIGHT is"<<(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT)<<std::endl;
	writer.open( "tst.avi",CV_FOURCC('M','J','P','G'), 30, size);

	if(argc == 1)
		cap.open( 0 );
	else
		return -1;
		//cap.open( argv[1] );
	if( !cap.isOpened() )
	{
		std::cerr<<"Couldn't open capture"<<std::endl;
		return -1;
	}
	
	cv::Mat frame;
	cv::Mat img_gry,img_cny;
	
	//std::cout<<"nChannels = "<<<<std::endl;
	if( !writer.isOpened() )
	{
		std::cerr<<"Couldn't open writer"<<std::endl;
		return -1;
	}

	while(1)
	{
		cap>>frame;
		if( !frame.data )break;
		cv::imshow("Cam-Original",frame);

		cv::cvtColor( frame, img_gry, CV_BGR2GRAY);
		cv::Canny( img_gry, img_cny, 10, 100, 3, 1);
		cv::imshow("Cam-Canny",img_cny);
		writer<<frame;
		char c = cv::waitKey(33);
		if( c== 27)
			break;
	}
	cap.release();
}
