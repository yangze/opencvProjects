#include <opencv2/opencv.hpp>

void example2_5(cv::Mat & image)
{
	cv::namedWindow("Example2_5-in",cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Example2_5-out",cv::WINDOW_AUTOSIZE);
// Create a window to show our input image 
	cv::imshow("Example2_5-in",image);	
// Create an image to hold the smoothed output
	cv::Mat out;
	cv::GaussianBlur(image,out,cv::Size(5,5),3,3);		
	cv::GaussianBlur(out,out,cv::Size(5,5),3,3);
	cv::imshow("Example2_5-out",out);
	cv::waitKey(0);
}

int main( int argc, char** argv )
{
	cv::Mat img = cv::imread(argv[1],-1);
	if( img.empty() ) return -1;
	example2_5(img);
}
