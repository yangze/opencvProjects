#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

int main(int argc, char** argv)
{
	cv::namedWindow("loadAVI",cv::WINDOW_AUTOSIZE);
	cv::VideoCapture cap;
	cap.open( (argv[1]));
	cv::Mat frame;
	while(1)
	{
		cap>>frame;
		if( !frame.data ) break;
		cv::imshow("loadAVI",frame);
		if( cv::waitKey(33)>=0 ) break;
	}
	return 0;
}
