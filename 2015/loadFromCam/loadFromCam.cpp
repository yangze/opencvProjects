#include <opencv2/opencv.hpp>
#include <iostream>

int main(int argc, char** argv)
{
	cv::namedWindow("Cam-Original",cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Cam-Canny",cv::WINDOW_AUTOSIZE);
	cv::VideoCapture cap;
	if(argc == 1)
		cap.open( 0 );
	else
		cap.open( argv[1] );
	if( !cap.isOpened() )
	{
		std::cerr<<"Couldn't open capture"<<std::endl;
		return -1;
	}
	cap.set( CV_CAP_PROP_FRAME_WIDTH,320 );
	cap.set( CV_CAP_PROP_FRAME_HEIGHT,240 );
	cv::Mat frame;
	cv::Mat img_gry,img_cny;
	while(1)
	{
		cap>>frame;
		if( !frame.data )break;
		cv::imshow("Cam-Original",frame);

		cv::cvtColor( frame, img_gry, CV_BGR2GRAY);
		cv::Canny( img_gry, img_cny, 10, 100, 3, 1);
		cv::imshow("Cam-Canny",img_cny);
		char c = cv::waitKey(33);
		if( c== 27)
			break;
	}
}
