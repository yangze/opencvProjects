#include <opencv2/opencv.hpp>

int main( int argc, char** argv)
{
	cv::Mat img_rgb = cv::imread( argv[1] );
	cv::Mat img_gry,img_cny;
	cv::cvtColor(img_rgb,img_gry,CV_BGR2GRAY);
	cv::namedWindow("Image-Gray",cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Image-Canny",cv::WINDOW_AUTOSIZE);

	cv::imshow("Image-Gray",img_gry);
	cv::Canny( img_gry, img_cny, 10, 100, 3, 1);

	cv::imshow("Image-Canny",img_cny);
	
	cv::waitKey(0);
}
