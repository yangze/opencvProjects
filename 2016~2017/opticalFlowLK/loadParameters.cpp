#include "loadParameters.h"

int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	{
		fs ["camera_matrix"] >> cameraMatrix;
		fs ["distortion_coefficients"] >> distCoeffs;
		printf("load parameters end\n");
		return 0;
	}
	return -1;
}
