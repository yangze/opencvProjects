#ifndef _LOADPARAMS_H
#define _LOADPARAMS_H
#include "opencv2/opencv.hpp"
#include <stdio.h>
using namespace cv;

int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs);
#endif
