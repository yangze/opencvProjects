#include "getOffset.h"
using namespace std;
using namespace cv;
/*float camearMatrix[3][3] = {
		{521.30318864889443,0,311.97476176497275},
		{0,519.20928294116004,235.74881935426956},
		{0,0,1},
};*/
Point2f offset_g;
float camearMatrix[3][3] = {
		{262.84399253523429,0,155.83098365908947},
		{0,261.72586728288718,118.34863882540270},
		{0,0,1},
};
int getOffset(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset,vector<float> &err,float *quality)
{
	int meancount = 0;
	double pixel_flow_x_mean = 0.0;
	double pixel_flow_y_mean = 0.0;
	double pixel_flow_x_integral = 0.0;
	double pixel_flow_y_integral = 0.0;
	double pixel_flow_x_stddev = 0.0;
	double pixel_flow_y_stddev = 0.0;
	//should undistort features points ...
	float pixel_flow_x_wight = 0.0;
	float pixel_flow_y_wight = 0.0;
	vector<Point2f> cornerIndexErr;
	
	for(int i=0;i<status.size();i++)
	{
		if(status[i] == 1)
		{
			pixel_flow_x_mean += pointCurrent[i].x-pointBefore[i].x;
			pixel_flow_y_mean += pointCurrent[i].y-pointBefore[i].y;
			meancount++;
		}
	}
	if(meancount)
	{
		pixel_flow_x_mean /= meancount;
		pixel_flow_y_mean /= meancount;
		//compute the flow variance
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				pixel_flow_x_stddev += (pointCurrent[i].x-pointBefore[i].x-pixel_flow_x_mean)*(pointCurrent[i].x-pointBefore[i].x-pixel_flow_x_mean);
				pixel_flow_y_stddev += (pointCurrent[i].y-pointBefore[i].y-pixel_flow_y_mean)*(pointCurrent[i].y-pointBefore[i].y-pixel_flow_y_mean);
			}
		}
		pixel_flow_x_stddev /= meancount;
		pixel_flow_y_stddev /= meancount;
		//convert to standard deviation
		pixel_flow_x_stddev = sqrt(pixel_flow_x_stddev);
		pixel_flow_y_stddev = sqrt(pixel_flow_y_stddev);
		//re-compute the mean flow with only the 95% consenting features
		meancount = 0;
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				double this_flow_x = pointCurrent[i].x-pointBefore[i].x;
				double this_flow_y = pointCurrent[i].y-pointBefore[i].y;
				if(abs(this_flow_x-pixel_flow_x_mean) <= 2*pixel_flow_x_stddev && abs(this_flow_y-pixel_flow_y_mean) <= 2*pixel_flow_y_stddev)
				{
					pixel_flow_x_integral += this_flow_x;
					pixel_flow_y_integral += this_flow_y;
					meancount++;
					//store valid corner to corner vector 
					cornerIndexErr.push_back(Point2f(i,err[i]));
				}
				else
				{
					//delete corner now
					//status[i] = 0;
				}
			}
		}
		if(cornerIndexErr.size() > 0)
		{
			float sumT = 0.0;
			for(int i=0;i<cornerIndexErr.size();i++)
			{
				sumT+=cornerIndexErr[i].y;
			}
			for(int i=0;i<cornerIndexErr.size();i++)
			{
				cornerIndexErr[i].y = cornerIndexErr[i].y / sumT;
				pixel_flow_x_wight += (pointCurrent[(int)cornerIndexErr[i].x].x-pointBefore[(int)cornerIndexErr[i].x].x)*cornerIndexErr[i].y;
				pixel_flow_y_wight += (pointCurrent[(int)cornerIndexErr[i].x].y-pointBefore[(int)cornerIndexErr[i].x].y)*cornerIndexErr[i].y;
			}
		
		}
		else
		{
			*quality = 0.0;
			return -1;
		}
		if(meancount)
		{
			pixel_flow_x_integral /= meancount;
			pixel_flow_y_integral /= meancount;
			/*offset.x = pixel_flow_x_integral;
			offset.y = pixel_flow_y_integral;
			*quality = (float)meancount/(float)status.size();
			return meancount;*/
			
			offset.x = pixel_flow_x_wight;
			offset.y = pixel_flow_y_wight;
			*quality = (float)cornerIndexErr.size()/(float)status.size();
			return cornerIndexErr.size();
		}
		else
		{
			*quality = 0.0;
			return -1;
		}
	}
	else// no data avaliable
	{
		*quality = 0.0;
		return -1;
	}
}

int sharpImage(Mat &gray,Mat &result)
{
	Mat kernel(3,3,CV_32F,Scalar(0));
	kernel.at<float>(1,1) = 5.0;
	kernel.at<float>(0,1) = -1.0;
	kernel.at<float>(1,0) = -1.0;
	kernel.at<float>(1,2) = -1.0;
	kernel.at<float>(2,1) = -1.0;
	
	filter2D(gray,result,gray.depth(),kernel);
}
int getMovement(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> status,Point2f &offset)
{
	float dx_sum = 0.0f;
	float dy_sum = 0.0f;
	int validPoints = 0;
	if(!pointBefore.empty() && !pointCurrent.empty() && !status.empty())
	{
		for(int i=0;i<pointCurrent.size();i++)
		{
			if(!status[i])
				continue;
			//printf("%0.f %0.f %0.f %0.f\n",pointCurrent[i].x,pointCurrent[i].y,pointBefore[i].x,pointBefore[i].y);
			dx_sum += pointCurrent[i].x-pointBefore[i].x;
			dy_sum += pointCurrent[i].y-pointBefore[i].y;
			validPoints++;
		}
		offset.x = dx_sum/validPoints;
		offset.y = dy_sum/validPoints;
		return 0;
	}
	return -1;
}
/*
* @param1: points before
* @param2: pointe after
* @param3: points flag
* @param4: offset (dx,dy)-Pixel
*/
int getMovement2(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset,vector<float> &err)
{
	int meancount = 0;
	double pixel_flow_x_mean = 0.0;
	double pixel_flow_y_mean = 0.0;
	double pixel_flow_x_integral = 0.0;
	double pixel_flow_y_integral = 0.0;
	double pixel_flow_x_stddev = 0.0;
	double pixel_flow_y_stddev = 0.0;
	float pixel_flow_x_wight = 0.0;
	float pixel_flow_y_wight = 0.0;
	//should undistort features points ...
	vector<Point2f> cornerIndexErr;
	for(int i=0;i<status.size();i++)
	{
		if(status[i] == 1)
		{
			pixel_flow_x_mean += pointCurrent[i].x-pointBefore[i].x;
			pixel_flow_y_mean += pointCurrent[i].y-pointBefore[i].y;
			meancount++;
		}
	}
	if(meancount)
	{
		pixel_flow_x_mean /= meancount;
		pixel_flow_y_mean /= meancount;
		//compute the flow variance
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				pixel_flow_x_stddev += (pointCurrent[i].x-pointBefore[i].x-pixel_flow_x_mean)*(pointCurrent[i].x-pointBefore[i].x-pixel_flow_x_mean);
				pixel_flow_y_stddev += (pointCurrent[i].y-pointBefore[i].y-pixel_flow_y_mean)*(pointCurrent[i].y-pointBefore[i].y-pixel_flow_y_mean);
			}
		}
		pixel_flow_x_stddev /= meancount;
		pixel_flow_y_stddev /= meancount;
		//convert to standard deviation
		pixel_flow_x_stddev = sqrt(pixel_flow_x_stddev);
		pixel_flow_y_stddev = sqrt(pixel_flow_y_stddev);
		//re-compute the mean flow with only the 95% consenting features
		meancount = 0;
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				double this_flow_x = pointCurrent[i].x-pointBefore[i].x;
				double this_flow_y = pointCurrent[i].y-pointBefore[i].y;
				if(abs(this_flow_x-pixel_flow_x_mean) <= 2*pixel_flow_x_stddev && abs(this_flow_y-pixel_flow_y_mean) <= 2*pixel_flow_y_stddev)
				{
					pixel_flow_x_integral += this_flow_x;
					pixel_flow_y_integral += this_flow_y;
					
					//store valid corner to corner vector 
					cornerIndexErr.push_back(Point2f(i,err[i]));
					meancount++;
				}
				else
				{
					//status[i] = 0;
				}
			}
		}
		if(cornerIndexErr.size() > 0)
		{
			float sumT = 0.0;
			for(int i=0;i<cornerIndexErr.size();i++)
			{
				//int index = (int)cornerIndexErr[i].x;
				sumT+=cornerIndexErr[i].y;
			}
			for(int i=0;i<cornerIndexErr.size();i++)
			{
				cornerIndexErr[i].y = cornerIndexErr[i].y / sumT;
				pixel_flow_x_wight += (pointCurrent[(int)cornerIndexErr[i].x].x-pointBefore[(int)cornerIndexErr[i].x].x)*cornerIndexErr[i].y;
				pixel_flow_y_wight += (pointCurrent[(int)cornerIndexErr[i].x].y-pointBefore[(int)cornerIndexErr[i].x].y)*cornerIndexErr[i].y;
			}
			
		}
		else
		{
			return 0;
		}
		if(meancount)
		{
			pixel_flow_x_integral /= meancount;
			pixel_flow_y_integral /= meancount;
			offset.x = pixel_flow_x_integral;
			offset.y = pixel_flow_y_integral;
			float flow_quality = 255.0*meancount/status.size();
			//printf("quality %.2f",flow_quality);
		}
	}
	
}
int offset2radians(float dx,float dy)
{
	float offset_x = dx;
	float offset_y = dy;
	float x_radians;
	float y_radians;
	
	if(offset_x < 0)
		offset_x = -offset_x;
	if(offset_y < 0)
		offset_y = -offset_y;
	if(offset_x < 1.0)
	{
		x_radians = 0.0;
	}
	else
	{
		x_radians = atan(offset_x/camearMatrix[0][0]);
	}
	if(offset_y < 1.0)
	{
		y_radians = 0.0;
	}
	else
	{
		y_radians = atan(offset_y/camearMatrix[1][1]);
	}

	
	printf(" ang %.4f %.4f\n",x_radians*180.0/PI*2,y_radians*180.0/PI*2);
	return 0;
}

int coor2radians(Point2f last,Point2f current,Point2f &radians)
{
	float x_last = last.x;
	float y_last = last.y;
	float x_curr = current.x;
	float y_curr = current.y;
	float alpha_x = 0.0;
	float beta_x = 0.0;
	float alpha_y = 0.0;
	float beta_y = 0.0;
	float offset_x = x_curr-x_last;
	float offset_y = y_curr-y_last;
	float offsetR_x = 0.0;
	float offsetR_y = 0.0;
	
	alpha_x = atan((abs(x_last-camearMatrix[0][2]))/camearMatrix[0][0]);
	beta_x = atan((abs(x_curr-camearMatrix[0][2]))/camearMatrix[0][0]);
	
	alpha_y = atan(abs((y_last-camearMatrix[1][2]))/camearMatrix[1][1]);
	beta_y = atan(abs((y_curr-camearMatrix[1][2]))/camearMatrix[1][1]);
	
	if(x_last < camearMatrix[0][2] && x_curr > camearMatrix[0][2] || x_last > camearMatrix[0][2] && x_curr < camearMatrix[0][2])
	{
		offsetR_x = (alpha_x)+(beta_x);
	}
	else
	{
		if(beta_x < alpha_x)
		{
			offsetR_x = alpha_x - beta_x;
		}
		else
		{
			offsetR_x = beta_x - alpha_x;
		}
	}
	
	if(y_last < camearMatrix[1][2] && y_curr > camearMatrix[1][2] || y_last > camearMatrix[1][2] && y_curr < camearMatrix[1][2])
	{
		offsetR_y = (alpha_y)+(beta_y);
	}
	else
	{
		if(beta_y < alpha_y)
		{
			offsetR_y = alpha_y - beta_y;
		}
		else
		{
			offsetR_y = beta_y - alpha_y;
		}
	}
	if(offset_x < 0)
	{
		offsetR_x = -offsetR_x;
	}
	if(offset_y < 0)
	{
		offsetR_y = -offsetR_y;
	}
	radians.x  = offsetR_x*180.0/PI*2.0;
	radians.y = offsetR_y*180.0/PI*2.0;
	//printf("r(%.3f,%.3f)\n",radians.x,radians.y);
	return 0;
}
/*
* Description: caculate radians of optical flow
* @param1: points before
* @param2: pointe current
* @param3: points flag
* @param4: offset (rx,ry)-Radians
*/
int points2radians(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset)
{
	int meancount = 0;
	double pixel_flow_radians_x_mean = 0.0;
	double pixel_flow_radians_y_mean = 0.0;
	double pixel_flow_radians_x_integral = 0.0;
	double pixel_flow_radians_y_integral = 0.0;
	double pixel_flow_radians_x_stddev = 0.0;
	double pixel_flow_radians_y_stddev = 0.0;
	
	vector<Point2f> radians_x_y;
	for(int i=0;i<status.size();i++)
	{
		Point2f tempRadians;
		coor2radians(pointBefore[i],pointCurrent[i],tempRadians);
		radians_x_y.push_back(tempRadians);
		
	}
	for(int i=0;i<status.size();i++)
	{	
		if(status[i] == 1)
		{
			pixel_flow_radians_x_mean += radians_x_y[i].x;
			pixel_flow_radians_y_mean += radians_x_y[i].y;
			meancount++;
		}
	}
	if(meancount)
	{
		pixel_flow_radians_x_mean /= meancount;
		pixel_flow_radians_y_mean /= meancount;
		//compute the flow variance
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				pixel_flow_radians_x_stddev += (radians_x_y[i].x-pixel_flow_radians_x_mean)*(radians_x_y[i].x-pixel_flow_radians_x_mean);
				pixel_flow_radians_y_stddev += (radians_x_y[i].y-pixel_flow_radians_y_mean)*(radians_x_y[i].y-pixel_flow_radians_y_mean);
			}
		}
		pixel_flow_radians_x_stddev /= meancount;
		pixel_flow_radians_y_stddev /= meancount;
		//convert to standard deviation
		pixel_flow_radians_x_stddev = sqrt(pixel_flow_radians_x_stddev);
		pixel_flow_radians_y_stddev = sqrt(pixel_flow_radians_y_stddev);
		//re-compute the mean flow with only the 95% consenting features
		meancount = 0;
		for(int i=0;i<status.size();i++)
		{
			if(status[i] == 1)
			{
				if(abs(radians_x_y[i].x-pixel_flow_radians_x_mean) <= 2*pixel_flow_radians_x_stddev && abs(radians_x_y[i].y-pixel_flow_radians_y_mean) <= 2*pixel_flow_radians_y_stddev)
				{
					pixel_flow_radians_x_integral += radians_x_y[i].x;
					pixel_flow_radians_y_integral += radians_x_y[i].y;
					meancount++;
				}
				else
				{
					status[i] = 0;
				}
			}
		}
		if(meancount)
		{
			pixel_flow_radians_x_integral /= meancount;
			pixel_flow_radians_y_integral /= meancount;
			offset.x = pixel_flow_radians_x_integral;
			offset.y = pixel_flow_radians_y_integral;
			float flow_quality = 255.0*meancount/status.size();
			//printf("quality %.2f",flow_quality);
		}
	}
	return 0;
}

int offset2radians2(Point2f offset,float distance,Point2f &radians)
{
	float alpha_x = 0.0;
	float alpha_y = 0.0;
	float offset_x = offset.x;
	float offset_y = offset.y;
	float cam_dx = 1.0/camearMatrix[0][0];
	float cam_dy = 1.0/camearMatrix[1][1];
	float d_x = offset_x/2.0/cam_dx;
	float d_y = offset_y/2.0/cam_dy;
	alpha_x = atan(d_x/distance);
	alpha_x +=alpha_x;
	
	alpha_y = atan(d_y/distance);
	alpha_y +=alpha_y;
	
	printf("mm y %.3f,%.0f ",d_y,offset_y);
	radians.x = alpha_x*180.0/PI;
	radians.y = alpha_y*180.0/PI;
	return 0;
}
int caclWinSum(Mat &gray,Point2f point,int suqareSize)
{
	int sum = 0;
	int numT = suqareSize/2;
	int width = gray.cols;
	int height = gray.rows;
	Point2i center; 
	int coor_x = 0;
	int coor_y = 0;
	center.x = (int)point.x;
	center.y = (int)point.y;
	coor_x = center.x - numT;
	coor_y = center.y - numT;
	
	if(center.x-numT<0 || center.x+numT>(width-1) || center.y-numT<0 || center.y+numT>(height-1) )
	{
#ifdef IDEBUG
		//printf("invalid operation\n");
#endif
		//printf("center1 (%d,%d)\n",center.x,center.y);
		return suqareSize*suqareSize*255;
	}
	for(int i=0;i<suqareSize;i++)
	{
		uint8_t *ptr = gray.ptr<uint8_t>(coor_y+i);
		for(int j=0;j<suqareSize;j++)
		{
			sum+=ptr[coor_x+j];
		}
	}
	return sum;
}
void deleteCorners(Mat &gray,vector<Point2f> &pointOriginal,vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,vector<int> &pointFeature)
{
	int i,k;
	for( i = k = 0; i < pointCurrent.size(); i++ )
    {
		if(abs(pointFeature[i]-caclWinSum(gray,pointCurrent[i],7)) > pointFeature[i]/3)
		{
			status[i] = 0;
			printf("delete corner\n");//delete current point
			printf("add corner\n");
			
			status[i] = 1;
			pointCurrent[i] = pointOriginal[i];
			pointFeature[i] = caclWinSum(gray,pointCurrent[i],7);
		}
		/*pointFeature[k] = pointFeature[i];
        pointCurrent[k] = pointCurrent[i];
        k++;*/
        //circle(gray,pointCurrent[i], 3, Scalar(0,255,0), -1, 8);
    }
	//pointCurrent.resize(k);
}
int addCorners(vector<Point2f> &pointOriginal,vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status)
{
	for( int i = 0; i < status.size(); i++ )
    {	
		if(status[i] == 0)
		{
			pointCurrent[i] = pointOriginal[i];
			//printf("add %.0f,%.0f\n",pointCurrent[i].x,pointCurrent[i].y);
		}
    }
    
	return 0;
}
int updateCorners(Mat &gray,vector<Point2f> &cornerNew,vector<uchar> &status,cornerInfo *cornerinfos,int infosize)
{
	if(!cornerinfos)
	{
		return -1;
	}
	int currentCorner = 0;
	int width = gray.cols;
	int height = gray.rows;
	int areaRow = 0 / SQUARESIZE;
	int areaCol = 0 % SQUARESIZE;
	int startRow = 0;
	int startCol = 0;
	//
	
	
	for(int i=0;i<infosize;i++)
	{
		if(status[i] == 0)
        {
			Point2f cornerT;
            if(fastCorner(gray,i,40,12,cornerT) == 0)
            {
                cornerNew[i] = cornerT;
                cornerinfos[i].flag = 1;
                cornerinfos[i].corner = cornerT;
                currentCorner++;
                //printf("init success\n");
            }
            else
            {
            	//fast corner can't find corner
            	/*addPointManual(gray,i,&cornerT);
            	cornerNew[i] = cornerT;
                cornerinfos[i].flag = 1;
                cornerinfos[i].corner = cornerT;*/
               
            }
        }
        else
        {
        	//seperate corners
			Point2f cornerNow = cornerNew[i];
			areaRow = i / SQUARESIZE;
			areaCol = i % SQUARESIZE;
	
			startRow = areaRow*SUBWIN_HEIGHT;//
			startCol = areaCol*SUBWIN_WIDTH; //x
			if(cornerNow.x >= startCol && cornerNow.x <= startCol+SUBWIN_WIDTH && cornerNow.y >= startRow && cornerNow.y <= startRow+SUBWIN_HEIGHT)
			{
				currentCorner++;
			}
			else
			{
				Point2f cornerT;
		        if(fastCorner(gray,i,40,12,cornerT) == 0)
		        {
		            cornerNew[i] = cornerT;
		            currentCorner++;
		            //printf("init success\n");
		        }
			}
        }
	}
	//printf("currentCorner %d\n",currentCorner);
	return currentCorner;
}
/*
* &desctiption:add point manually
* @param1: gray image
* @param2: sub window id
* @oaram3: cornerInfoArray size
*
*/
int addPointManual(Mat &gray,int id,Point2f *point)
{
	if(!point)
		return -1;

	int width = gray.cols;
	int height = gray.rows;
	int areaRow = id / SQUARESIZE;
	int areaCol = id % SQUARESIZE;
	int startRow = 0;
	int startCol = 0;
	
	startRow = areaRow*SUBWIN_HEIGHT;
	startCol = areaCol*SUBWIN_WIDTH;
	
	point->x = (startCol+(startCol+SUBWIN_WIDTH))/2;
	point->y = (startRow+(startRow+SUBWIN_HEIGHT))/2;
	
	return 0;
}
/*
* @param1: gray image
* @param2: sub window id
* @oaram3: cornerInfoArray size
*
*/
int fastCorner(Mat &gray,int id,int grayThreshold,int countThreshold,Point2f &outCorner)
{
	int width = gray.cols;
	int height = gray.rows;
	int areaRow = id / SQUARESIZE;
	int areaCol = id % SQUARESIZE;
	int startRow = 0;
	int startCol = 0;
	vector<cornerInfo> cornerArrayT;
	vector<KeyPoint> keypoints;
	//
	startRow = areaRow*SUBWIN_HEIGHT;
	startCol = areaCol*SUBWIN_WIDTH;
	//opencv fast
	Rect rect(startCol,startRow,SUBWIN_WIDTH,SUBWIN_HEIGHT);
    Mat gray_roi = gray(rect);
    FAST(gray_roi,keypoints,grayThreshold);
    if(keypoints.size() == 0)
    {
        return -1;
    }
    else
    {
        //pick out corners
        outCorner.x = keypoints[keypoints.size()/2].pt.x+startCol;
        outCorner.y = keypoints[keypoints.size()/2].pt.y+startRow;
#ifdef SUBDRAW
        for(int i=0;i<keypoints.size();i++)
        {
            Point2f point;
            point.x = keypoints[i].pt.x+startCol;
            point.y = keypoints[i].pt.y+startRow;
            
            circle( gray,point, 3, Scalar(0,255,0), -1, 8);
        }
        printf("size %d\n",keypoints.size());
#endif
        return 0;
    }

}

void drawResult(Mat &gray,vector<CvPoint> corners_all,vector<CvPoint> corners_nms)
{
	for(unsigned int i=0; i < corners_all.size(); i++)
	{
		//cvLine( gray, cvPoint( corners_all[i].x, corners_all[i].y ), cvPoint( corners_all[i].x, corners_all[i].y ), CV_RGB(255,0,0) );
		 line(gray, Point(corners_all[i].x, corners_all[i].y), Point(corners_all[i].x, corners_all[i].y), CV_RGB(255,0,0));
	}
	/*for(unsigned int i=0; i < corners_nms.size(); i++ )
	{
		//points
		cvLine( gray, cvPoint( corners_nms[i].x, corners_nms[i].y ), cvPoint( corners_nms[i].x, corners_nms[i].y ), CV_RGB(0,255,0) );
	}*/
}
void drawResult2(Mat &img,CvPoint *corner,int num)
{
	for(int i=0;i<num;i++)
	{
		circle(img,corner[i],1,Scalar(0,255,0),-1,8);
	}
}
void drawResult3(Mat &img,vector<KeyPoint> &corner)
{
	for(int i=0;i<corner.size();i++)
	{
		circle(img,Point(corner[i].pt.x,corner[i].pt.y),1,Scalar(0,255,0),-1,8);
	}
}
