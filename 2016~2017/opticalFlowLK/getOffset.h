#ifndef __GET_OFFSET_H
#define __GET_OFFSET_H

#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <math.h>

#define PI 3.1415
/*
* Attention: SQUARESIZE^2 = AREANUM
*/
#define AREANUM 64
#define SQUARESIZE 8

#define IMAGE_WIDTH 320
#define IMAGE_HEIGHT 240

#define SUBWIN_HEIGHT IMAGE_HEIGHT/SQUARESIZE
#define SUBWIN_WIDTH IMAGE_WIDTH/SQUARESIZE

//#define SUBDRAW

#define IMUDATANUM 4
#pragma pack(1) 

typedef struct imu
{
	uint8_t pack_head_chk[2];
	int64_t time_stamp;
	float array[IMUDATANUM];
	uint16_t crc;
}imu_data_t;
#pragma pack()

using namespace cv;

typedef struct corner_Info
{
	bool flag;
	int areaID;
	int quality;
	Point2f corner;
}cornerInfo;
int getMovement(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> status,Point2f &offset);
int getMovement2(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset,vector<float> &err);
int offset2radians(float dx,float dy);
int coor2radians(Point2f last,Point2f current,Point2f &radians);
int points2radians(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset);
int offset2radians2(Point2f offset,float distance,Point2f &radians);
int caclWinSum(Mat &gray,Point2f center,int suqareSize);
void deleteCorners(Mat &gray,vector<Point2f> &pointOriginal,vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,vector<int> &pointFeature);
int addCorners(vector<Point2f> &pointOriginal,vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status);
int updateCorners(Mat &gray,vector<Point2f> &cornerNew,vector<uchar> &status,cornerInfo *cornerinfos,int infosize);
int addPointManual(Mat &gray,int id,Point2f *point);
int fastCorner(Mat &gray,int id,int grayThreshold,int countThreshold,Point2f &outCorner);

int getOffset(vector<Point2f> &pointBefore,vector<Point2f> &pointCurrent,vector<uchar> &status,Point2f &offset,vector<float> &err,float *quality);


//image prpcessing
int sharpImage(Mat &gray,Mat &result);
void drawResult(Mat &gray,vector<CvPoint> corners_all,vector<CvPoint> corners_nms);
void drawResult2(Mat &img,CvPoint *corner,int num);
void drawResult3(Mat &img,vector<KeyPoint> &corner);
#endif
