#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <iostream>
#include <ctype.h>
#include "Itime.h"
#include "getOffset.h"
#include "loadParameters.h"
#include "agast5_8.h"
#include "agast7_12s.h"
#include "agast7_12d.h"
#include "oast9_16.h"
#include "cvfast.h"
#define MAX_COUNT 20


using namespace cv;
using namespace std;

//threshold for accelerated segment test (16-, 12- and 8-pixel mask)
#define AST_THR_16	40
#define AST_THR_12	38
#define AST_THR_8	27

enum AST_PATTERN {OAST9_16, AGAST7_12d, AGAST7_12s, AGAST5_8, AST_PATTERN_LENGTH};

extern Point2f offset_g;
Point2f point;
bool addRemovePt = false;
Point2f viewOffset;
Point2f point_posi_ori;
vector<Point2f> pointsOriginal;
Point2f radians;
vector<int> pointFeature(MAX_COUNT); //record
cornerInfo cornerInfoArray[AREANUM];

vector<Point2f> cornerOld(AREANUM);
vector<Point2f> cornerNew(AREANUM);
vector<uchar> status(AREANUM);

static void onMouse( int event, int x, int y, int /*flags*/, void* /*param*/ )
{
    if( event == EVENT_LBUTTONDOWN )
    {
        point = Point2f((float)x, (float)y);
        point_posi_ori = point;
        pointsOriginal.push_back(point);
        addRemovePt = true;
    }
}
static void drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step,
                    double, const Scalar& color)
{
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const Point2f& fxy = flow.at<Point2f>(y, x);
            line(cflowmap, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),
                 color);
            circle(cflowmap, Point(x,y), 2, color, -1);
        }
}
int main( int argc, char** argv )
{
    VideoCapture cap(0);
    ITimer timer;
    int64_t startT,endT;
    float quality = 0.0;
    TermCriteria termcrit(TermCriteria::COUNT|TermCriteria::EPS,20,0.03);
    Size subPixWinSize(10,10), winSize(31,31);
    bool needToInit = true;

    if( !cap.isOpened() )
    {
        cout << "Could not initialize capturing...\n";
        return 0;
    }
    namedWindow( "gray", 1 );
    namedWindow( "sharp", 1 );
    Mat gray, prevGray, image, frame;
    Mat sharpGray;
    vector<Point2f> points[2];

	vector<Point2f> goodFeatures;
	vector<KeyPoint> keypoints;
	//
	FILE *file,*filex,*filey;
	float dx,dy;
	file = fopen("log.dat","w+");;
	filex = fopen("logxLK.dat","w+");
	filey = fopen("logyLK.dat","w+");
	
	// init parameters
	cap.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	cap.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
	memset(&cornerInfoArray,0,sizeof(cornerInfoArray));
	for(int i=0;i<AREANUM;i++)
	{
		cornerInfoArray[i].flag = 0;
		cornerInfoArray[i].areaID = i;
		status[i] = 0;
	}
	// save data to file
	file = fopen("video2.dat","w+");
	while(0)
	{
		cap >> frame;
        if( !frame.data )
        {
            printf("error\n");
            break;
        }

        frame.copyTo(image);
        cvtColor(image, gray, COLOR_BGR2GRAY);
        IplImage iplGray = gray;
		CvPoint* corners;
		int numCorners;
    	startT = timer.gettime();
    	
    	//cvCornerFast(&iplGray, 30, 11, 0, &numCorners, & corners);
		//FAST(gray,keypoints,30,false);
		//AGAST(gray,keypoints,30):
		/*OastDetector9_16 ad9_16(320, 240, AST_THR_16);
		ad9_16.processImage((uint8_t*)gray.data);
		vector<CvPoint> corners_all=ad9_16.get_corners_all();
		vector<CvPoint> corners_nms=ad9_16.get_corners_nms();*/
		endT = timer.gettime();
		//drawResult2(image,corners,numCorners);

		//drawResult(image,corners_all,corners_nms);
		drawResult3(image,keypoints);
		printf("cost %lld\n",endT-startT);
        imshow("gray", image);
		char c = (char)waitKey(30);
        if( c == 27 )
            return -1;
		
	}
	Mat flow,uflow,cflow;
	while(0)
	{
		cap >> frame;
        if( !frame.data )
        {
            printf("error\n");
            break;
        }

        frame.copyTo(image);
        cvtColor(image, gray, COLOR_BGR2GRAY);
        if(prevGray.empty())
                gray.copyTo(prevGray);
        IplImage prev = prevGray;
		IplImage curr = gray;
		
        //
        startT = timer.gettime();
		/*calcOpticalFlowFarneback(prevGray,gray,uflow,0.1, 3, 7, 3, 5, 1.2, 0);
        endT = timer.gettime();
     	cvtColor(prevGray, cflow, COLOR_GRAY2BGR);
        uflow.copyTo(flow);
        drawOptFlowMap(flow, cflow, 16, 1.5, Scalar(0, 255, 0));*/
        
        /*for(int i=0;i<50;i++)
        {
        	uint8_t *ptr = tempx.ptr<uint8_t>(i);
        	for(int j=0;j<50;j++)
        	{
        		printf("%d ",ptr[j]);
        	}
        	printf("\n");
        }*/
        printf("cost %lld\n",endT-startT);
        imshow("gray", cflow);
		
        char c = (char)waitKey(10);
        if( c == 27 )
            return 0;
        
		//store old image
        cv::swap(prevGray, gray);
	}
	
    for(;;)
    {
        cap >> frame;
        if( !frame.data )
        {
            printf("error\n");
            break;
        }
		Mat sharpTmp;
        frame.copyTo(image);
        cvtColor(image, gray, COLOR_BGR2GRAY);
        gray.copyTo(sharpGray);
		//sharpImage(gray,sharpTmp);
		//sharpTmp.copyTo(gray);
		Point2f cornerT;
        if( needToInit)
        {
            // automatic initialization
            int ret = updateCorners(gray,cornerNew,status,cornerInfoArray,AREANUM);
            cornerOld = cornerNew;
            printf("init goodFeature ok number = %d\n",ret);
            needToInit = 0;
            //pointsOriginal = points[1];

            addRemovePt = false;
        }
        else if( !cornerOld.empty() && 1)
        {
        	int currentCorner = 0;
            vector<float> err;
            if(prevGray.empty())
                gray.copyTo(prevGray);

	        calcOpticalFlowPyrLK(prevGray, gray, cornerOld, cornerNew, status, err, winSize,
	                             3, termcrit, OPTFLOW_LK_GET_MIN_EIGENVALS, 0.001);
			//getMovement2(cornerOld,cornerNew,status,viewOffset,err);
			getOffset(cornerOld, cornerNew,status,viewOffset,err,&quality);
			dx = viewOffset.x;
            dy = viewOffset.y;
            //fwrite(&tag,sizeof(tag),1,file);
			fwrite(&dx,sizeof(dx),1,filex);
            fwrite(&dy,sizeof(dy),1,filey);
            
			currentCorner = updateCorners(gray,cornerNew,status,cornerInfoArray,AREANUM);
			if(currentCorner < AREANUM/3)
			{
				//cvCalcOpticalFlowBM();
			}
			for(int i=0;i<status.size();i++)
            {
                /*if(status[i] == 1)
                {
                	char buf[15];
                	string label;
                	sprintf(buf,"err %.3f",i,err[i]);
                	label.append(buf);
                    putText(image,label,cornerNew[i],CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 0));
                    circle( gray, cornerNew[i], 3, Scalar(0,255,0), -1, 8);
                }
                else
                {
                    circle( image, cornerNew[i], 3, Scalar(0,0,255), -1, 8);
                }*/
                if(status[i] == 1)
             		circle( gray, cornerNew[i], 3, Scalar(0,255,0), -1, 8);
            }
		}
		
        needToInit = false;
        imshow("gray", gray);
        imshow("sharp",sharpGray);
		/*if(points[1].size() < MAX_COUNT/2)
        {
        	needToInit = true;
        	printf("points loss %d\n",points[0].size());
        }*/
        char c = (char)waitKey(10);
        if( c == 27 )
            break;

        std::swap(cornerNew, cornerOld);
        cv::swap(prevGray, gray);
    }
	fclose(file);
    fclose(filex);
    fclose(filey);
    return 0;
}
