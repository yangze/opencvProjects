#!/bin/bash
cameraID=1
if [ -z "$1" ]
then
	echo "invalid input parameters"
	echo "run eg:1:./run.sh calib or 2:./run undis"
	exit 1
fi

make


case "$1" in
"calib")
	./camera 0 0
	;;
"undis")	
	./camera 1 0 cameraParam.xml
	;;
esac

# param1:0 camera calibration 1 camera undistortion
# param2:0 -> camera 1,   1 -> camera 2
# eg: run calibration
#./camera 0 0

#eg: run undistortion
#./camera 1 0 cameraParam.xml
./recover
