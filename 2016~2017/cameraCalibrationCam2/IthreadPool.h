#ifndef __THREADTEST_H
#define __THREADTEST_H
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifdef __WIN32
#include <windows.h>
#include <unistd.h>
#endif // __WIN32

#include "ILog.h"

typedef struct worker
{
    void *(*process) (void *arg);   //thread process
    void *arg;                      // thread process arguments
    struct worker *next;
}Thread_worker;

typedef struct
{
    pthread_mutex_t queue_lock;
    pthread_cond_t queue_ready;

    Thread_worker *queue_head;      //all thread saved in a linked list
    int shutdown;                   // destroy thread pool or not flag
    pthread_t *threadid;
    int max_thread_num;             // the number of max process that have access to work
    int cur_queue_size;             // current queue number

}Thread_pool;

class IThreadPool
{
public:
    IThreadPool(int threadMax);
    ~IThreadPool();
    int pool_add_worker(void *(*process)(void *arg), void *arg);

private:
    int pool_init(int max_thread_num);
    static void * thread_routine (void *arg);
    void run_main_routine();
protected:
    Thread_pool *pool;
};
#endif // __THREADTEST_H
