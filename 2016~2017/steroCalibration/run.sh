#!/bin/bash

if [ -z "$1" ]
then
	echo "invalid input parameters"
	echo "run eg:1:./run.sh calib or 2:./run undis"
	exit 1
fi

make


case "$1" in
"calib")
	./steroCamera 0 cameraParamL.xml cameraParamR.xml
	;;
"undis")	
	./steroCamera 1 cameraParamL.xml cameraParamR.xml extrinsics.yml
	;;
esac

# param1:0 camera calibration 1 camera undistortion
# param2:0 -> camera 1,   1 -> camera 2
# eg: run calibration
#./camera 0 0

#eg: run undistortion
#./camera 1 0 cameraParam.xml
./recover
