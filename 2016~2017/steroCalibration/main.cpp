#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "opencv2/opencv.hpp"
#include "getKeyVal.h"
#include "takePicture.h"
using namespace cv;
using namespace std;
#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480

#define W_NUM 9
#define H_NUM 6

#define SAM_FRAMES 10


#define N_TOP		409
#define N_BOTTOM	320
#define N_LEFT		640
#define N_RIGHT		543

#define N_WIDTH 1280
#define N_HEIGHT 960
//

#define CAM_L_ID 1
#define CAM_R_ID 0
pthread_t ops_thread;

static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners );

char option_g = 0;
void *scan_keyboard_thread(void *p)
{
	int exit_flag = 1;//quit this process flag
	char option = 0;
    set_disp_mode(STDIN_FILENO,0);
	while(exit_flag)
    {
        while(kbhit() == 0);
        option = getchar();

        switch(option)
        {
            case 'n'://exopsure gain add
            		option_g = 'n';
                     break;
         	case 'q':
                    exit_flag = 0;
                    option_g = 'q';
                    set_disp_mode(STDIN_FILENO,1); // recover original attribute of terminal
                     break;
         	case 'r':
         			option_g = 'r';
         			break;
            case 's':
                    option_g = 's';
                     break; 
			case 'p':
                    option_g = 'p';
                     break;
        }
    }
    pthread_exit(NULL);
}

int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	{
		fs ["camera_matrix"] >> cameraMatrix;
		fs ["distortion_coefficients"] >> distCoeffs;
		return 0;
	}
	return -1;
}
int loadStereoParams(const char * path,Mat &R,Mat &T,Mat &Rl,Mat &Rr,Mat &Pl,Mat &Pr,Mat &Q)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	if(fs.isOpened())
	{
		fs["R"] >> R;
		fs["T"] >> T;
		fs["Rl"] >> Rl;
		fs["Rr"] >> Rr;
		fs["Pl"] >> Pl;
		fs["Pr"] >> Pr;
		fs["Q"] >> Q;
		return 0;
	}
	return -1;
}
static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners )
{
    corners.resize(0);

    for( int i = 0; i < boardSize.height; i++ )
        for( int j = 0; j < boardSize.width; j++ )
            corners.push_back(Point3f(float(j*squareSize),float(i*squareSize), 0));
}
int runCalibration(vector<vector<Point2f> >imagePointsL,vector<vector<Point2f> >imagePointsR,
					Mat cameraMatrixL,Mat distCoeffsL,Mat cameraMatrixR,Mat distCoeffsR,
					Size imageSize,Size boardSize,Mat& R,Mat& T,Mat& E,Mat& F)
{
	const float squareSize = 26.5f;  // Set this to your actual square size
	vector<vector<Point3f> > objectPoints(1);

	calcChessboardCorners(boardSize, squareSize, objectPoints[0]);
	objectPoints.resize(imagePointsL.size(),objectPoints[0]);
	
	stereoCalibrate(objectPoints,imagePointsL,imagePointsR,cameraMatrixL,distCoeffsL,
					cameraMatrixR,distCoeffsR,imageSize,R,T,E,F,
					cvTermCriteria(CV_TERMCRIT_ITER+
        CV_TERMCRIT_EPS, 100, 1e-5));
	return 0;
}
static const char *cameraLParameters = "cameraParamL.xml";
static const char *cameraRParameters = "cameraParamR.xml";

void help()
{
	printf("run eg:./steroCamera 0 cameraParamL.xml cameraParamR.xml\n");
	printf("cmd 0...\n");
}
int main(int argc,char **argv)
{
	
	if(argc < 4)
	{
		printf("input parameter error\n");
		help();
		return -1;
	}
	
	//cmd temp
	int cmd = atoi(argv[1]);
	bool have_calibrated_flag = false;
	VideoCapture capL(CAM_L_ID);
	VideoCapture capR(CAM_R_ID);
	
	Size imageSizeL,imageSizeR;
	Size newImageSizeL,newImageSizeR;
	
	vector<vector<Point2f> > imagePointsL;
	vector<vector<Point2f> > imagePointsR;
	vector<Point2f> cornersBufL;
	vector<Point2f> cornersBufR;
	
	bool cornersFindFlagL = false;
	bool cornersFindFlagR = false;
	
	Mat frameL,frameR;
	Mat grayL,grayR;
	Mat grayTmpL,grayTmpR;
	Mat map1L,map2L,map1R,map2R;
	Mat newLeft,newRight;
	Mat rviewROIL,rviewROIR;
	Mat disparityL,disparityR;
	
	Mat cameraMatrixL, distCoeffsL;
	Mat cameraMatrixR, distCoeffsR;
	
	//out put params
	Mat R,T,E,F;
	Mat Rl, Rr, Pl, Pr, Q;
	Mat mapL1,mapL2,mapR1,mapR2;
	Rect validRoi[2];
	imageSizeL.width = IMAGE_WIDTH;
	imageSizeL.height = IMAGE_HEIGHT;
	
	imageSizeR.width = IMAGE_WIDTH;
	imageSizeR.height = IMAGE_HEIGHT;
	Mat imgDisparity16S = Mat(imageSizeL.width,imageSizeL.height,CV_16S );
	Mat imgDisparity8U = Mat(imageSizeL.width,imageSizeL.height, CV_8UC1 );

	newImageSizeL.width = 1280;
	newImageSizeL.height = 960;
	//-- 2. Call the constructor for StereoBM
	int ndisparities = 16*5;   /**< Range of disparity */
	int SADWindowSize = 15; /**< Size of the block window. Must be odd */

	StereoBM sbm(StereoBM::BASIC_PRESET,ndisparities,SADWindowSize);
	
	if(argv[2] && argv[3])
	{
		int ret = 0;
		if(access(argv[2],F_OK) < 0 || access(argv[3],F_OK) < 0)
		{
			printf("can not find file %s   %s \n",argv[2],argv[3]);
			return -1;
		}
		//load left camera params
		ret = loadCameraParams(argv[2],cameraMatrixL, distCoeffsL);
		if(ret < 0)
		{
			printf("load %s error for left cameras\n",argv[3]);
			return -1;
		}
#ifdef DEBUG
		cout<<"left M"<<cameraMatrixL<<endl;
		cout<<"left D"<<distCoeffsL<<endl;
#endif
		//load right camera params
		ret = loadCameraParams(argv[3],cameraMatrixR, distCoeffsR);
		if(ret < 0)
		{
			printf("load %s error for right cameras\n",argv[3]);
			return -1;
		}
#ifdef DEBUG
		cout<<"right M"<<cameraMatrixR<<endl;
		cout<<"right D"<<distCoeffsR<<endl;
#endif
	}
	
	if(cmd)
		have_calibrated_flag = true;
	if(!capL.isOpened())
	{
		printf("can not open left camera\n");
		return -1;
	}
	if(!capR.isOpened())
	{
		printf("can not open right camera\n");
		return -1;
	}
	printf("open camera ID:0 ID:1 success\n");

	capL.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	capL.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
	capR.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	capR.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
	
	namedWindow("left",WINDOW_NORMAL);
	namedWindow("right",WINDOW_NORMAL);
	resizeWindow("left",IMAGE_WIDTH,IMAGE_HEIGHT);
	resizeWindow("right",IMAGE_WIDTH,IMAGE_HEIGHT);
	
	pthread_create(&ops_thread,NULL,scan_keyboard_thread,NULL);// key value get process
	if(have_calibrated_flag)
	{
		int ret = loadStereoParams(argv[4],R,T,Rl,Rr,Pl,Pr,Q);
		if(ret < 0)
		{
			printf("read parameters error\n");
			return -1;
		}
		//
		initUndistortRectifyMap(cameraMatrixL, distCoeffsL,Rl,Pl,newImageSizeL,CV_16SC2,mapL1,mapL2);
		initUndistortRectifyMap(cameraMatrixR, distCoeffsR,Rr,Pr,newImageSizeL,CV_16SC2,mapR1,mapR2);
#if 0
		cout<<"R"<<R<<endl;
		cout<<"T"<<T<<endl;
		cout<<"Rl"<<Rl<<endl;
		cout<<"Rr"<<Rr<<endl;
#endif
	}
	while(1)
	{
		vector<Point2f> cornersL;
		vector<Point2f> cornersR;
		bool foundL;
		bool foundR;
		capL >> frameL;
		capR >> frameR;
		if(!frameL.data || !frameR.data)
		{
			printf("get invalid frame data\n");
			break;
		}
		cvtColor(frameL,grayL,CV_BGR2GRAY);
		cvtColor(frameR,grayR,CV_BGR2GRAY);
		//find chess board corners
		if(!have_calibrated_flag)
		{
			foundL = findChessboardCorners(grayL,Size(W_NUM,H_NUM),cornersL,CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE+CALIB_CB_FAST_CHECK);
			foundR = findChessboardCorners(grayR,Size(W_NUM,H_NUM),cornersR,CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE+CALIB_CB_FAST_CHECK);
			if(foundL && foundR)
			{
				cornerSubPix(grayL,cornersL,Size(11, 11),Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS+ CV_TERMCRIT_ITER,30,0.1));
				drawChessboardCorners(frameL,Size(W_NUM,H_NUM),Mat(cornersL),foundL);
				
				cornerSubPix(grayR,cornersR,Size(11, 11),Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS+ CV_TERMCRIT_ITER,30,0.1));
				drawChessboardCorners(frameR,Size(W_NUM,H_NUM),Mat(cornersR),foundR);
				if(option_g == 'n')
				{
					option_g = 0;
					//save corners
					imagePointsL.push_back(cornersL);
					imagePointsR.push_back(cornersR);
					printf("current points size %d\n",(int)imagePointsL.size());
				}
				if(imagePointsL.size() >= SAM_FRAMES)
				{
					have_calibrated_flag = true;
					//excute stero calibration here
					printf("start stero calibration\n");
					//stereoCalibrate();
					runCalibration(imagePointsL,imagePointsR,cameraMatrixL, distCoeffsL,
									cameraMatrixR, distCoeffsR,imageSizeL,Size(W_NUM,H_NUM),R,T,E,F);
					stereoRectify(cameraMatrixL, distCoeffsL,
									cameraMatrixR, distCoeffsR,
									imageSizeL,R,T,Rl,Rr,Pl,Pr,Q,
									CALIB_ZERO_DISPARITY,0,newImageSizeL,&validRoi[0], &validRoi[1]);
					initUndistortRectifyMap(cameraMatrixL, distCoeffsL,Rl,Pl,newImageSizeL,CV_16SC2,mapL1,mapL2);
					initUndistortRectifyMap(cameraMatrixR, distCoeffsR,Rr,Pr,newImageSizeL,CV_16SC2,mapR1,mapR2);
				}
			}
		}
		if(have_calibrated_flag)
		{
			//-- Check its extreme values
			double minVal; double maxVal;

			minMaxLoc( imgDisparity16S, &minVal, &maxVal );
			remap(grayL,newLeft,mapL1,mapL2,INTER_LINEAR);
			remap(grayR,newRight,mapR1,mapR2,INTER_LINEAR);
			sbm(newLeft,newRight,imgDisparity16S,CV_16S);
			imgDisparity16S.convertTo(imgDisparity8U,CV_8UC1, 255/(maxVal - minVal));
		}
		if(have_calibrated_flag)
		{
			imshow("left",imgDisparity8U);
			imshow("right",newRight);
		}
		else
		{
			imshow("left",frameL);
			imshow("right",frameR);
		}
		
		waitKey(30);
		if(option_g == 's')//save all calibrated parameters
		{
			option_g = 0;
			FileStorage fs("extrinsics.yml", CV_STORAGE_WRITE);
			if( fs.isOpened() )
			{
				fs<<"R"<<R<<"T"<<T<<"Rl"<<Rl<<"Rr"<<Rr<<"Pl"<<Pl<<"Pr"<<Pr<<"Q"<<Q;
				fs.release();
				cout<<"calibrated parameters have save in file extrinsics.yml"<<endl;
			}
			else
				cout << "Error: can not save the intrinsic parameters\n";
		}
		if(option_g == 'r')//recalibration process
		{
			option_g = 0;
			imagePointsL.clear();
			imagePointsR.clear();
			have_calibrated_flag = false;
		}
	
		if(option_g == 'p')
		{
			option_g = 0;
			char fileName[20] = {0};
			int index = 0;
		
			
			sprintf(fileName,"eulerPictureL.png");
			vector<int> compression_params;
			compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
			compression_params.push_back(9);
			imwrite(fileName,newLeft,compression_params);
			sprintf(fileName,"eulerPictureR.png");
			imwrite(fileName,newRight,compression_params);
		}
		
		if(option_g == 'q')//end process
		{
			pthread_join(ops_thread,NULL);
			printf("process exit \n");
			break;
		}
	}
	return 0;
}
