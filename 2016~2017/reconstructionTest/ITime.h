#ifndef _ITIME_H
#define _ITIME_H
#include <time.h>
#include <stdint.h>
#include <unistd.h>

int64_t get_time();
#endif //end _ITIME_H
