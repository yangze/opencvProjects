#include <iostream>
#include <stdio.h>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/nonfree/nonfree.hpp"

#include "opencv2/core/core_c.h"
#include "cv.h"

#include "ITime.h"
using namespace std;
using namespace cv;

static void help()
{
    printf("Help:input example./reconstrucrion <image1> <image2>\n");
}

int main(int argc,char **argv)
{
	IplImage * left, *right;
	if(argc != 3)
	{
		help();
		return -1;
	}
	cvNamedWindow("show",1);
	
	int SADWindowSize=15;
	int64_t start,end;
	left = cvLoadImage(argv[1],CV_LOAD_IMAGE_COLOR);
	right = cvLoadImage(argv[2],CV_LOAD_IMAGE_COLOR);
	
	IplImage* dst_left = cvCreateImage(cvGetSize(left),left->depth,1);
	IplImage* dst_right = cvCreateImage(cvGetSize(left),left->depth,1);
	
	cvCvtColor(left,dst_left,CV_BGR2GRAY);
	cvCvtColor(right,dst_right,CV_BGR2GRAY);
	
	CvMat* left_disp_ = cvCreateMat(left->height, left->width, CV_32FC1);
	CvMat* left_vdisp = cvCreateMat(left->height, left->width,CV_8UC1);
	
	CvStereoBMState *BMState = cvCreateStereoBMState();
	BMState->SADWindowSize = SADWindowSize > 0 ? SADWindowSize : 9;  
	BMState->minDisparity = 0;  
	BMState->numberOfDisparities = 32;  
	BMState->textureThreshold = 10;  
	BMState->uniquenessRatio = 15;  
	BMState->speckleWindowSize = 100;  
	BMState->speckleRange = 32;  
	BMState->disp12MaxDiff = 1;
	start = get_time();
	cvFindStereoCorrespondenceBM( dst_left, dst_right, left_disp_,BMState);
	cvNormalize( left_disp_, left_vdisp, 0, 256, CV_MINMAX );
	end = get_time();
	printf("time cost %lld\n",end-start);
	cvShowImage("show",left_vdisp);
	cvWaitKey(0);
	return 0;
}
