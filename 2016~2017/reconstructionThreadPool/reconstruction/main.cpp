#include <iostream>
#include <cv.h>
#include "IthreadPool.h"
#include "Itime.h"
extern "C"
{
void *runThread(void *)
{
    int64_t sum;
    ITimer timer;
	int64_t start;
	int64_t end;
	//timer.TimerStart();
	start = timer.gettime();
    for(int i = 0;i < 10000;i++)
        for(int j=0;j < 65535;j++)
            sum++;
	end = timer.gettime();
	LOG("cost time 1 %f sec\n",(float)(end-start)/1000000.0);
	//double tm = timer.TimerFinish();
	//LOG("thread 1 cost %f\n",tm);
}
void *runThread2(void *argv)
{
    int64_t sum;
    ITimer timer;
	int64_t start;
	int64_t end;
	//timer.TimerStart();
	start = timer.gettime();
    for(int i = 0;i < 10000;i++)
        for(int j=0;j < 65535;j++)
            sum++;
	end = timer.gettime();
	LOG("cost time 2 %f sec\n",(float)(end-start)/1000000.0);
	//double tm = timer.TimerFinish();
	//LOG("thread 2 cost %f\n",tm);
}
void *runThread3(void *argv)
{
	int64_t sum;
	ITimer timer;
	int64_t start;
	int64_t end;
	//timer.TimerStart();
	start = timer.gettime();
	for(int i = 0;i < 10000;i++)
		for(int j=0;j < 65535;j++)
			sum++;
	end = timer.gettime();
	LOG("cost time 3 %f sec\n",(float)(end-start)/1000000.0);
	//double tm = timer.TimerFinish();
	//LOG("thread 2 cost %f\n",tm);
}
}
using namespace std;

int main(int argc,char **argv)
{
    if(argc != 3)
    {
        printf("input parameter error\n");
    }
	int64_t sum;
	int64_t start;
	int64_t end;

    ITimer timer;
	IThreadPool pool(3);
	pool.pool_add_worker(runThread,NULL);
	pool.pool_add_worker(runThread2,NULL);
	pool.pool_add_worker(runThread3,NULL);
	//Sleep(1000*1);
	//timer.TimerStart();
	start = timer.gettime();
	for(int i = 0;i < 10000;i++)
		for(int j=0;j < 65535;j++)
			sum++;
	for(int i = 0;i < 10000;i++)
		for(int j=0;j < 65535;j++)
			sum++;
	end = timer.gettime();
	LOG("cost time %f sec\n",(float)(end-start)/1000000.0);
	//double tm = timer.TimerFinish();
	//printf("cost %f\n",tm);
    return 0;
}
