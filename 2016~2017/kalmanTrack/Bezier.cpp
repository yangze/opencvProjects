#include "Bezier.h"

Bezier::Bezier()
:dataNum(0)
{
	memset(controlPoints,0,sizeof(controlPoints));
	
}
Bezier::Bezier(IPoint2f &point1,IPoint2f &point2,IPoint2f &point3,IPoint2f &point4,float step)
{
	initBezier(point1,point2,point3,point4,step);
}
int Bezier::initBezier(IPoint2f &point1,IPoint2f &point2,IPoint2f &point3,IPoint2f &point4,float step)
{
	dataNum = 0;
	float tTemp = 0.0f;
	float x0 = point1.x;
	float y0 = point1.y;
	float x1 = point4.x;
	float y1 = point4.y;
	float x11 = point2.x;
	float y11 = point2.y;
	float x21 = point3.x;
	float y21 = point3.y;
	
	this->step = step;
	controlPoints[0] = point1;
	controlPoints[1] = point2;
	controlPoints[2] = point3;
	controlPoints[3] = point4;
	for(int i=0;i<MAXDATANUM;i++)
	{
		curve[i].x = (-x0+3*x11-3*x21+x1)*powf(tTemp,3.0f) + (3*x0-6*x11+3*x21)*powf(tTemp,2.0f)+(-3*x0+3*x11)*tTemp+x0;
		curve[i].y = (-y0+3*y11-3*y21+y1)*powf(tTemp,3.0f) + (3*y0-6*y11+3*y21)*powf(tTemp,2.0f)+(-3*y0+3*y11)*tTemp+y0;
		tTemp+=this->step;
		dataNum++;
		if(tTemp >= 1.0f)
		{
			break;
		}
	}
	return 0;
}
Bezier::Bezier(float points[8])
{
	
}
Bezier::~Bezier()
{
	
}

