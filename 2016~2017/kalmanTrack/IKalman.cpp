#include "IKalman.h"

using namespace cv;
motion_estimator::motion_estimator()
{
	statePre = Mat::zeros(2,1,CV_32F);
	statePost = Mat::zeros(2,1,CV_32F);
	
	A = *(Mat_<float>(2, 2) << 1, 1, 0, 1);
	//No control parameters
	//B = ;
	Q = Mat::eye(2,2,CV_32F);
	R = Mat::eye(1,1,CV_32F);
	
	H = Mat::zeros(1,2,CV_32F);
	
	errorCovPre = Mat::zeros(2,2,CV_32F);
	errorCovPost = Mat::zeros(2,2,CV_32F);
	
	k = Mat::zeros(2,1,CV_32F);
	
	temp1.create(2,2,CV_32F);
	temp2.create(1,2,CV_32F);
	temp3.create(1,1,CV_32F);
	temp4.create(1,2,CV_32F);
	temp5.create(1,1,CV_32F);
}
motion_estimator::~motion_estimator()
{
	
}
	
int motion_estimator::init()
{
	setIdentity(Q,Scalar::all(1e-5));
	setIdentity(R,Scalar::all(1e-1));
}
Mat &motion_estimator::predict()
{
	//update the state : x'(k) = A * x(k)
	statePre = A*statePost;
	//update error covariance matrices: temp1 = A*P(k)
	temp1 = A*errorCovPost;
	//P'(k) = temp1*At + Q
	gemm(temp1,A,1,Q,1,errorCovPre,GEMM_2_T);
	
	//
	statePre.copyTo(statePost);
	errorCovPre.copyTo(errorCovPost);
	return statePre;
}
Mat &motion_estimator::correct(const Mat &measurement)
{
	//temp2 = H*P'(k)
	temp2 = H * errorCovPre;
	//temp3 = temp2*ht + R
	gemm(temp2,H,1,R,1,temp3,GEMM_2_T);
	//temp4 = inv(temp3)*temp2 = Kt(k)
	solve(temp3,temp2,temp4,DECOMP_SVD);
	//K(k)
	k = temp4.t();
	//temp5 = z(k) - H*x'(k)
	temp5 = measurement - H*statePre;
	
	//x(k) = x'(k) + K(x)*temp5
	statePost = statePre + k*temp5;
	
	//P(k) = P'(k) - K(k)*temp2
	errorCovPost = errorCovPre - k*temp2;
	
	return statePost;
}
