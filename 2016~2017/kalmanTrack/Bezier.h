#pragma once
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include<math.h>

#include "dataTypes.h"

#define CURVEORDER 3
#define CONTLPTNUM CURVEORDER+1

#define MAXDATANUM 1024
class Bezier
{
public:
	Bezier();
	Bezier(IPoint2f &point1,IPoint2f &point2,IPoint2f &point3,IPoint2f &point4,float step = 0.1);
	Bezier(float points[8]);
	~Bezier();
	int initBezier(IPoint2f &point1,IPoint2f &point2,IPoint2f &point3,IPoint2f &point4,float step);
public:
	IPoint2f controlPoints[CONTLPTNUM];
	int dataNum;
	float step;
	float t[MAXDATANUM];//Range 0~1
	IPoint2f curve[MAXDATANUM];
};
