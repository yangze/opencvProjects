#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "Bezier.h"
#include "IKalman.h"
#include "Ikalman2.h"
#include "dataTypes.h"
#include "getOffset.h"
#include "Itime.h"

#define LOGFLOW
#define MAP_WIDTH 2000
#define MAP_HEIGHT 2000
using namespace cv;
using namespace Algorithm;
using namespace std;

IKalmanFilter KF_X(2,1,0); //dx direction
IKalmanFilter KF_Y(2,1,0); //dy direction
IKalmanFilter KF_POSx(2,1,0); //dy direction
IKalmanFilter KF_POSy(2,1,0); //dy direction

//optical flow parameters
vector<Point2f> cornerOld(AREANUM);
vector<Point2f> cornerNew(AREANUM);
vector<uchar> status(AREANUM);
cornerInfo cornerInfoArray[AREANUM];
Point2f viewOffset;
Point2f historyMove;
Point2f historyMoveKalman;
Point2f mapCenter;
int main(int argc,char **argv)
{
	Mat map(MAP_HEIGHT,MAP_WIDTH,CV_8UC3);
	VideoCapture cap(0);
	Mat gray, prevGray,image,frame;
    bool needToInit = true;
    float quality = 0.0;
    
    ITimer timer;
    int64_t sampleStart = 0;
    int64_t sampleEnd = 0;
    TermCriteria termcrit(TermCriteria::COUNT|TermCriteria::EPS,20,0.03);
    Size subPixWinSize(10,10), winSize(31,31);
    
    FILE *file,*filex,*filey,*filext,*fileyt;
	float dx,dy;
	file = fopen("log.dat","w+");
	
	filext = fopen("logx.dat","w+");
	fileyt = fopen("logy.dat","w+");
	
	filex = fopen("logxLK.dat","w+");
	filey = fopen("logyLK.dat","w+");
	
	if(!cap.isOpened())
	{
		printf("Opencv /dev/video0 failed\n");
		return -1;
	}
	namedWindow("map",0);
	resizeWindow("map",500,500);
	moveWindow("map",1500,0);
	
	namedWindow("OpticalFlow",0);
	resizeWindow("OpticalFlow",500,500);
	moveWindow("OpticalFlow",900,0);
	// init optical flow parameters
	cap.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	cap.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
	memset(&cornerInfoArray,0,sizeof(cornerInfoArray));
	for(int i=0;i<AREANUM;i++)
	{
		cornerInfoArray[i].flag = 0;
		cornerInfoArray[i].areaID = i;
		status[i] = 0;
	}
	historyMove.x = MAP_WIDTH/2.0;
	historyMove.y = MAP_HEIGHT/2.0;
	mapCenter = historyMove; 
	//Kalman filter init(optical flow)
    matrix statex(2,1),statey(2,1);
    matrix processNoisex(2,1),processNoisey(2,1);
    matrix measurementx(1,1),measurementy(1,1);
    matrix resultx(2,1),resulty(2,1);
    //Kalman filter init(camera position)
	matrix statePosx(2,1),statePosy(2,1);
    matrix processNoisePos(2,1);
    matrix measurementPosx(1,1),measurementPosy(1,1);
    matrix resultPos(2,1);
    
	//init kalman filter  image dx
	KF_X.A.data[0] = 1.0;
    KF_X.A.data[1] = 1.0;
    KF_X.A.data[2] = 0.0;
    KF_X.A.data[3] = 1.0;
    
    KF_X.H.data[0] = 1.0;
   	KF_X.H.data[1] = 0.0;
	KF_X.Q*=0.02;	//init as eye matrix
	KF_X.R*=3;
	
	KF_X.errorCovPost.eye(KF_X.errorCovPost.m,KF_X.errorCovPost.n);
	
	//init kalman filter  image dy
	KF_Y.A.data[0] = 1.0;
    KF_Y.A.data[1] = 1.0;
    KF_Y.A.data[2] = 0.0;
    KF_Y.A.data[3] = 1.0;
    
	KF_Y.H.data[0] = 1.0;
   	KF_Y.H.data[1] = 0.0;
	KF_Y.Q*=0.02;
	KF_Y.R*=3;
	
	KF_Y.errorCovPost.eye(KF_Y.errorCovPost.m,KF_Y.errorCovPost.n);
	
	//camera position estimator
	KF_POSx.A.data[0] = 1.0;
    KF_POSx.A.data[1] = 1.0;
    KF_POSx.A.data[2] = 0.0;
    KF_POSx.A.data[3] = 1.0;
    
    KF_POSx.H.data[0] = 1.0;
   	KF_POSx.H.data[1] = 0.0;
	KF_POSx.Q*=0.02;	//init as eye matrix
	KF_POSx.R*= 0.05;
	
	KF_POSx.errorCovPost.eye(KF_POSx.errorCovPost.m,KF_POSx.errorCovPost.n);
	
	KF_POSy.A.data[0] = 1.0;
    KF_POSy.A.data[1] = 1.0;
    KF_POSy.A.data[2] = 0.0;
    KF_POSy.A.data[3] = 1.0;
    
    KF_POSy.H.data[0] = 1.0;
   	KF_POSy.H.data[1] = 0.0;
	KF_POSy.Q*=0.02;	//init as eye matrix
	KF_POSy.R*=0.05;
	
	KF_POSy.errorCovPost.eye(KF_POSy.errorCovPost.m,KF_POSy.errorCovPost.n);
	
	while(1)
	{
		cap >> frame;
		cvtColor(frame,gray,COLOR_BGR2GRAY);
		
		if(needToInit)
		{
			// automatic initialization
            int ret = updateCorners(gray,cornerNew,status,cornerInfoArray,AREANUM);
            cornerOld = cornerNew;
            printf("init goodFeature ok number = %d\n",ret);
			needToInit = false;
		}
		else if( !cornerOld.empty())
        {
        	int currentCorner = 0;
        	int ret = 0;
            vector<float> err;
            if(prevGray.empty())
                gray.copyTo(prevGray);
			sampleStart = timer.gettime();
	        calcOpticalFlowPyrLK(prevGray, gray, cornerOld, cornerNew, status, err, winSize,
	                             3, termcrit, OPTFLOW_LK_GET_MIN_EIGENVALS, 0.001);
			ret = getOffset(cornerOld, cornerNew,status,viewOffset,err,&quality);
			if(ret < 5)
			{
				//historyMove.x = MAP_WIDTH/2.0;
				//historyMove.y = MAP_HEIGHT/2.0;
			}
			matrix predictX = KF_X.predict();
			matrix predictY = KF_Y.predict();
			matrix predictPosx = KF_POSx.predict();
			matrix predictPosy = KF_POSy.predict();
			
			Mat measurementTempx(1,1,CV_32F);
			Mat measurementTempy(1,1,CV_32F);
			randn(measurementTempx, Scalar::all(0), Scalar::all(KF_X.R.data[0]));
			measurementx.data[0] = measurementTempx.at<float>(0);
			randn(measurementTempy, Scalar::all(0), Scalar::all(KF_Y.R.data[0]));
			measurementy.data[0] = measurementTempy.at<float>(0);
			
			//get measurement value
			
			//update measurent data
			//measurementx.data[0] += historyMove.x;
			//measurementy.data[0] += historyMove.y;
			measurementx.data[0] = viewOffset.x;
			measurementy.data[0] = viewOffset.y;
			
			statex = KF_X.correct(measurementx);
			statey = KF_Y.correct(measurementy);
			//create process noise
			
			historyMove.x += statex.data[0];
			historyMove.y += statey.data[0];
			
			//position estimator measurement data update
			measurementPosx.data[0] = historyMove.x;
			measurementPosy.data[0] = historyMove.y;
			
			statePosx = KF_POSx.correct(measurementPosx);
			statePosy = KF_POSy.correct(measurementPosy);
			
			historyMoveKalman.x = statePosx.data[0];
			historyMoveKalman.y = statePosy.data[0];
			
			//
			Point2f pointCorrect(historyMove.x,historyMove.y);
			
			//map = Scalar::all(0);
			circle(map,historyMove,4,Scalar(0,100,200),4);
			circle(map,mapCenter,4,Scalar(0,255,0),4);
			circle(map,pointCorrect,4,Scalar(0,0,255),4);
			circle(map,historyMoveKalman,4,Scalar(0,255,0),4);
			#ifdef LOGFLOW
			dx = historyMove.x;
            dy = historyMove.y;
            //fwrite(&tag,sizeof(tag),1,file);
            float ddx,ddy;
            ddx = historyMoveKalman.x;
            ddy = historyMoveKalman.y;
            fwrite(&ddx,sizeof(ddx),1,filext);
            fwrite(&ddy,sizeof(ddy),1,fileyt);
            
			fwrite(&dx,sizeof(dx),1,filex);
            fwrite(&dy,sizeof(dy),1,filey);
            #endif
			currentCorner = updateCorners(gray,cornerNew,status,cornerInfoArray,AREANUM);
			if(currentCorner < AREANUM/3)
			{

			}
			for(int i=0;i<status.size();i++)
            {
                if(status[i] == 1)
             		circle( gray, cornerNew[i], 3, Scalar(0,255,0), -1, 8);
            }
		}

		imshow("OpticalFlow",gray);
		imshow("map",map);
		std::swap(cornerNew, cornerOld);
        cv::swap(prevGray, gray);
        
        sampleEnd = timer.gettime();
		printf("cost %lld\n",sampleEnd-sampleStart);
        
		char code = (char)waitKey(1);
		if( code == 27 || code == 'q' || code == 'Q' )
			break;
		else if(code == 'c')
		{
			historyMove.x = MAP_WIDTH/2.0;
			historyMove.y = MAP_HEIGHT/2.0;
			map = Scalar::all(0);
		}
	}
	return 0;
}
