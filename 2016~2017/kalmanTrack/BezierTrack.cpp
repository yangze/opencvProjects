#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "Bezier.h"
#include "IKalman.h"
#include "Ikalman2.h"
#include "dataTypes.h"


using namespace cv;
using namespace Algorithm;
using namespace std;

//Bezier curve
int pointsCounts = 0;
bool curveflag = false;
IPoint2f p1 = {0,0};
IPoint2f p2 = {0,0};
IPoint2f p3 = {0,0};
IPoint2f p4 = {0,0};

Bezier bezier;

//Motion estimator
IKalmanFilter KF_X(2,1,0); //dx direction
IKalmanFilter KF_Y(2,1,0); //dy direction


static void onMouse( int event, int x, int y, int /*flags*/, void* /*param*/ )
{
    if( event == CV_EVENT_LBUTTONDOWN )
    {
        Point2f point = Point2f((float)x, (float)y);
        
        pointsCounts++;
        
        switch(pointsCounts)
        {
        	case 1:
        		p1.x = point.x;
        		p1.y =  point.y;
        	break;
        		
        	case 2:
        		p2.x = point.x;
        		p2.y =  point.y;
        	break;
        	case 3:
        		p3.x = point.x;
        		p3.y =  point.y;
        	break;
        	case 4:
        		p4.x = point.x;
        		p4.y =  point.y;
        		pointsCounts = 0;
        		bezier.initBezier(p1,p2,p3,p4,0.01);
        		curveflag = true;
        	break;
        }
    }
}

void showBezier(Mat &img)
{
	for(int i=0;i<bezier.dataNum;i++)
	{
		Point2f pointT;
		pointT.x = bezier.curve[i].x;
		pointT.y = bezier.curve[i].y;
		circle(img,pointT,1,Scalar(255,255,255),1);
	}
}

int main(int argc,char **argv)
{
	Mat img(500, 500, CV_8UC3);
	int trackPointsNum = 0;
	namedWindow( "BezierTrace", 0 );
    setMouseCallback( "BezierTrace", onMouse, 0 );
    //
    matrix statex(2,1),statey(2,1);
    matrix processNoisex(2,1),processNoisey(2,1);
    matrix measurementx(1,1),measurementy(1,1);
    matrix resultx(2,1),resulty(2,1);
	//init kalman filter  image dx
	KF_X.A.data[0] = 1.0;
    KF_X.A.data[1] = 1.0;
    KF_X.A.data[2] = 0.0;
    KF_X.A.data[3] = 1.0;
    
    KF_X.H.data[0] = 1.0;
   	KF_X.H.data[1] = 0.0;
	KF_X.Q*=0.02;	//init as eye matrix
	KF_X.R*=5;
	
	KF_X.errorCovPost.eye(KF_X.errorCovPost.m,KF_X.errorCovPost.n);
	
	//init kalman filter  image dy
	KF_Y.A.data[0] = 1.0;
    KF_Y.A.data[1] = 1.0;
    KF_Y.A.data[2] = 0.0;
    KF_Y.A.data[3] = 1.0;
    
	KF_Y.H.data[0] = 1.0;
   	KF_Y.H.data[1] = 0.0;
	KF_Y.Q*=0.02;
	KF_Y.R*=5;
	
	KF_Y.errorCovPost.eye(KF_Y.errorCovPost.m,KF_Y.errorCovPost.n);
	
	while(1)
	{
		if(curveflag)
		{
			//
			matrix predictX = KF_X.predict();
			matrix predictY = KF_Y.predict();
			//measurement data
			Mat measurementTempx(1,1,CV_32F);
			Mat measurementTempy(1,1,CV_32F);
		
			randn(measurementTempx, Scalar::all(0), Scalar::all(KF_X.R.data[0]));
			measurementx.data[0] = measurementTempx.at<float>(0);
		
			randn(measurementTempy, Scalar::all(0), Scalar::all(KF_Y.R.data[0]));
			measurementy.data[0] = measurementTempy.at<float>(0);
			//generate mesaurement zk = H * xk + R
		
			//measurementx = measurementx + KF_X.H*statex;
			//measurementy = measurementy + KF_Y.H*statey;
			measurementx.data[0] += bezier.curve[trackPointsNum].x;
			measurementy.data[0] += bezier.curve[trackPointsNum].y;
		
			Point2f point(measurementx.data[0],measurementy.data[0]);
		    
		    circle(img,point,2,Scalar(0,255,255),2);
		    
			statex = KF_X.correct(measurementx);
			statey = KF_Y.correct(measurementy);
		
			Point2f pointCorrect(statex.data[0],statey.data[0]);
			circle(img,pointCorrect,2,Scalar(0,100,200),2);
			
			//create process noise
			Mat processNoiseTempx(2,1,CV_32F);
			Mat processNoiseTempy(2,1,CV_32F);
		
			randn( processNoiseTempx, Scalar(0), Scalar::all(sqrt(KF_X.Q.data[0])));
		    processNoisex.data[0] = processNoiseTempx.at<float>(0);
		    processNoisex.data[1] = processNoiseTempx.at<float>(1);
		    
		    randn( processNoiseTempy, Scalar(0), Scalar::all(sqrt(KF_Y.Q.data[0])));
		    processNoisey.data[0] = processNoiseTempy.at<float>(0);
		    processNoisey.data[1] = processNoiseTempy.at<float>(1);
		    
		    //statex = KF_X.A*statex + processNoisex;
		    //statey = KF_Y.A*statey + processNoisey;
		    
		    
			if(curveflag)
			{
				showBezier(img);
			}
			else
			{
				//img = Scalar::all(0);
			}
			trackPointsNum++;
			if(trackPointsNum > bezier.dataNum)
			{
				trackPointsNum = 0;
				img = Scalar::all(0);
				showBezier(img);
			}
		}
		imshow("BezierTrace",img);
		char code = (char)waitKey(100);
		if( code == 27 || code == 'q' || code == 'Q' )
			break;
	}
	return 0;
}
