#pragma once
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

typedef struct _point2i
{
	int16_t x;
	int16_t y;
}IPoint2i;

typedef struct _point2f
{
	float x;
	float y;
}IPoint2f;
