#include "opencv2/video/tracking.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <stdio.h>
#include <iostream>
#include "IKalman.h"
#include "Ikalman2.h"
using namespace cv;
using namespace Algorithm;
using namespace std;
static inline Point calcPoint(Point2f center, double R, double angle)
{
    return center + Point2f((float)cos(angle), (float)-sin(angle))*(float)R;
}

static void help()
{
    printf( "\nExamle of c calls to OpenCV's Kalman filter.\n"
"   Tracking of rotating point.\n"
"   Rotation speed is constant.\n"
"   Both state and measurements vectors are 1D (a point angle),\n"
"   Measurement is the real point angle + gaussian noise.\n"
"   The real and the estimated points are connected with yellow line segment,\n"
"   the real and the measured points are connected with red line segment.\n"
"   (if Kalman filter works correctly,\n"
"    the yellow segment should be shorter than the red one).\n"
            "\n"
"   Pressing any key (except ESC) will reset the tracking with a different speed.\n"
"   Pressing ESC will stop the program.\n"
            );
}

motion_estimator estimator;

int main(int, char**)
{
	IKalmanFilter KF(2,1,0);

    help();
    Mat img(500, 500, CV_8UC3);

    //Mat state(2, 1, CV_32F); /* (phi, delta_phi) */
    //Mat processNoise(2, 1, CV_32F);
    //Mat measurement = Mat::zeros(1, 1, CV_32F);
    matrix state(2,1);
    matrix processNoise(2,1);
    matrix measurement(1,1);
    matrix result(2,1);
    char code = (char)-1;

    for(;;)
    {
    	Mat stateTmp(2,1,CV_32F);
        randn( stateTmp, Scalar::all(0), Scalar::all(0.1) );
		state.data[0] = stateTmp.at<float>(0);
		state.data[1] = stateTmp.at<float>(1);
		
        setIdentity(estimator.H,Scalar::all(1));

        setIdentity(estimator.Q, Scalar::all(1e-5));
        setIdentity(estimator.R, Scalar::all(1e-1));
        setIdentity(estimator.errorCovPost, Scalar::all(1)); 
        
        //Ikalmanfilter init propcess
        KF.A.data[0] = 1;
        KF.A.data[1] = 10.0;
        KF.A.data[2] = 0;
        KF.A.data[3] = 1;
       
       	KF.H.data[0] = 1.0;
       	KF.H.data[1] = 0.0;
		KF.Q*=1e-5;
		KF.R*=1e-5;
		
		KF.errorCovPost.eye(KF.errorCovPost.m,KF.errorCovPost.n);
		
        KF.statePost.data[0] = 0.0;
        KF.statePost.data[1] = 0.0;
        for(;;)
        {
            Point2f center(img.cols*0.5f, img.rows*0.5f);
            
            float R = img.cols/3.f;
            double stateAngle = state.data[0];
            Point statePt = calcPoint(center, R, stateAngle);
			
			matrix predict;
            Mat prediction/* = estimator.predict()*/;
            predict = KF.predict();
            double predictAngle = predict.data[0];
            Point predictPt = calcPoint(center, R, predictAngle);

            //randn( measurement, Scalar::all(0), Scalar::all(estimator.R.at<float>(0)));
			//randn( measurement, Scalar::all(0), Scalar::all(KF.R.data[0]));
			Mat measurementTemp(1,1,CV_32F);
			randn(measurementTemp, Scalar::all(0), Scalar::all(KF.R.data[0]));

			measurement.data[0] = measurementTemp.at<float>(0);
			
            // generate measurement zk = H * xk + R
            //measurement += estimator.H*state;
			measurement +=KF.H*state;
            double measAngle = measurement.data[0];
            Point measPt = calcPoint(center, R, measAngle);

            // plot points
            #define drawCross( center, color, d )                                 \
                line( img, Point( center.x - d, center.y - d ),                \
                             Point( center.x + d, center.y + d ), color, 1, CV_AA, 0); \
                line( img, Point( center.x + d, center.y - d ),                \
                             Point( center.x - d, center.y + d ), color, 1, CV_AA, 0 )

            img = Scalar::all(0);
            drawCross( statePt, Scalar(255,255,255), 3 );
            //drawCross( measPt, Scalar(0,0,255), 3 );
            drawCross( predictPt, Scalar(0,255,0), 3 );
            //line( img, statePt, measPt, Scalar(0,0,255), 3, CV_AA, 0 );
           // line( img, statePt, predictPt, Scalar(0,255,255), 3, CV_AA, 0 );

            //if(theRNG().uniform(0,4) != 0)
            result = KF.correct(measurement);
			double resultAngle = result.data[0];
			Point resultPt = calcPoint(center, R, resultAngle);
            
			//drawCross( resultPt, Scalar(255,255,0), 3 );
			
            //randn( processNoise, Scalar(0), Scalar::all(sqrt(KF.Q.data[0])));
            Mat processNoiseTemp(2,1,CV_32F);
            randn( processNoiseTemp, Scalar(0), Scalar::all(sqrt(KF.Q.data[0])));
            processNoise.data[0] = processNoiseTemp.at<float>(0);
            processNoise.data[1] = processNoiseTemp.at<float>(1);
            
            state = KF.A*state + processNoise;
            
            
            imshow( "Kalman", img );
            code = (char)waitKey(100);

            if( code > 0 )
                break;
        }
        if( code == 27 || code == 'q' || code == 'Q' )
            break;
    }

    return 0;
}
