#pragma once
#include <opencv2/opencv.hpp>
#include <stdio.h>
class motion_estimator
{
public:
	motion_estimator();
	~motion_estimator();
	
	int init();
	int update();
	cv::Mat &predict();
	cv::Mat &correct(const cv::Mat &measurement);
	cv::Mat x;
	cv::Mat A; //state-transition model
	cv::Mat B; //the control input model
	cv::Mat H; //the obervation model (maps the true space into observed space)
	
	cv::Mat Q; //the covariance of the process noise
	cv::Mat R; //the covariance of the observarion noise
	
	cv::Mat statePre;
	cv::Mat statePost;
	
	cv::Mat errorCovPre;		//p'(k)
	cv::Mat errorCovPost; 		//p(k)
	cv::Mat k;				//k
	
	cv::Mat temp1;
	cv::Mat temp2;
	cv::Mat temp3;
	cv::Mat temp4;
	cv::Mat temp5;
};
