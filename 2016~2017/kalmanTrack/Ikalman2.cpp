#include "Ikalman2.h"

namespace Algorithm
{
	IKalmanFilter::IKalmanFilter()
	{
		
	}
	IKalmanFilter::~IKalmanFilter()
	{
	
	}
	
	
	IKalmanFilter::IKalmanFilter(int dynamParams,int measureParams,int controlParams)
	{
		init(dynamParams,measureParams,controlParams);
	}
	void IKalmanFilter::init(int DP,int MP,int CP)
	{
		//statePre
		statePre.m = DP;
		statePre.n = 1;
		//statePost
		statePost.m = DP;
		statePost.n = 1;
		//A
		A.m = DP;
		A.n = DP;
		A.eye(A.m,A.n);
		
		//Q
		Q.m = DP;
		Q.n = DP;
		Q.eye(Q.m,Q.n);
		//H
		H.m = MP;
		H.n = DP;
		H.zero();
		//R
		R.m = MP;
		R.n = MP;
		R.eye(R.m,R.n);
		
		//errorCovPre
		errorCovPre.m = DP;
		errorCovPre.n = DP;
		errorCovPre.zero();
		//errorCovPost
		errorCovPost.m = DP;
		errorCovPost.n = DP;
		errorCovPost.zero();
		//K
		K.m = DP;
		K.n = MP;
		K.zero();
		
		if(CP > 0)
		{
		
		}
		else
		{
			
		}
	}
	const matrix& IKalmanFilter::predict(const matrix& control)
	{
		//update the state : x'(k) = A * x(k)
		statePre = A*statePost;
		errorCovPre = A * errorCovPost * A.transpos() + Q;
		
		statePost = statePre;
		errorCovPost = errorCovPre;
		return statePre;
	}
	const matrix& IKalmanFilter::correct(matrix& measurement)
	{
		matrix Sk = H * errorCovPre * H.transpos() + R;
		//k = 
		K = errorCovPre * H.transpos() * Sk.inverse();
		
		//x(k) = x'(k) + K(x)*(z(k)-H*x'(k))
		statePost = statePre + K*(measurement - H*statePre);
		
		//P(k) = P'(k) - K(k)*temp2
		errorCovPost = errorCovPre - K*H*errorCovPre;
		return statePost;
	}
}
