#pragma once
#include <stdint.h>
#include <stdio.h>
#include "matrix.h"

namespace Algorithm
{

class IKalmanFilter
{
public:
	IKalmanFilter();
	~IKalmanFilter();
	IKalmanFilter(int dynamParams,int measureParams,int controlParams = 0);
	void init(int dynamParams,int measureParams,int controlParams = 0);
	const matrix& predict(const matrix& control = matrix());
	const matrix& correct(matrix& measurement);
	
	matrix x;
	matrix A; //state-transition model
	matrix B; //the control input model
	matrix H; //the obervation model (maps the true space into observed space)
	
	matrix Q; //the covariance of the process noise
	matrix R; //the covariance of the observarion noise
	
	matrix statePre; //predict state (x'(k)): x(k)=A*x(k-1)+B*u(k)
	matrix statePost; //corrected state  (x(k)): x(k)=x'(k)+K(k)*(z(k)-H*x'(k))
	
	matrix errorCovPre;		//p'(k)
	matrix errorCovPost; 		//p(k)
	matrix K;				//k
	
	matrix P1;
	matrix temp1;
	matrix temp2;
	matrix temp3;
	matrix temp4;
	matrix temp5;
	
};

}
