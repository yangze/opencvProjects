#include "opencv2/opencv.hpp"

#include <iostream>
#include <stdio.h>
using namespace cv;
using namespace std;

static void help()
{
    cout <<
            "\nThis program demonstrates dense optical flow algorithm by Gunnar Farneback\n"
            "Mainly the function: calcOpticalFlowFarneback()\n"
            "Call:\n"
            "./fback\n"
            "This reads from video camera 0\n" << endl;
}
static void drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step,
                    double, const Scalar& color)
{
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const Point2f& fxy = flow.at<Point2f>(y, x);
            line(cflowmap, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),
                 color);
            circle(cflowmap, Point(x,y), 2, color, -1);
        }
}
int getoffset2(Mat &flow,Point2f &offset)
{
	int rows = flow.rows;
	int cols = flow.cols;
	vector<float> offset_x;
	vector<float> offset_y;
	
	int meancount = 0;
	double pixel_flow_x_mean = 0.0;
	double pixel_flow_y_mean = 0.0;
	double pixel_flow_x_integral = 0.0;
	double pixel_flow_y_integral = 0.0;
	double pixel_flow_x_stddev = 0.0;
	double pixel_flow_y_stddev = 0.0;
	float pixel_flow_x_wight = 0.0;
	float pixel_flow_y_wight = 0.0;

	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<cols;j++)
		{
			Point2f fxy = flow.at<Point2f>(i,j);
			if(fxy.x > 1.5 || fxy.y > 1.5)
			{
				offset_x.push_back(fxy.x);
				offset_y.push_back(fxy.y);
			}
		}
	}
	
	for(int i=0;i<offset_x.size();i++)
	{
		pixel_flow_x_mean += offset_x[i];
		pixel_flow_y_mean += offset_y[i];
		meancount++;
	}
	
	if(meancount)
	{
		pixel_flow_x_mean /= meancount;
		pixel_flow_y_mean /= meancount;
		//compute the flow variance
		for(int i=0;i<offset_x.size();i++)
		{
			pixel_flow_x_stddev += (offset_x[i]-pixel_flow_x_mean)*(offset_x[i]-pixel_flow_x_mean);
			pixel_flow_y_stddev += (offset_y[i]-pixel_flow_y_mean)*(offset_y[i]-pixel_flow_y_mean);
		}
		pixel_flow_x_stddev /= meancount;
		pixel_flow_y_stddev /= meancount;
		//convert to standard deviation
		pixel_flow_x_stddev = sqrt(pixel_flow_x_stddev);
		pixel_flow_y_stddev = sqrt(pixel_flow_y_stddev);
		//re-compute the mean flow with only the 95% consenting features
		meancount = 0;
		for(int i=0;i<offset_x.size();i++)
		{
			if(abs(offset_x[i]-pixel_flow_x_mean) <= 2*pixel_flow_x_stddev && abs(offset_y[i]-pixel_flow_y_mean) <= 2*pixel_flow_y_stddev)
			{
				pixel_flow_x_integral += offset_x[i];
				pixel_flow_y_integral += offset_y[i];
				
				meancount++;
			}
			else
			{
				//status[i] = 0;
			}
			
		}
		if(meancount)
		{
			pixel_flow_x_integral /= meancount;
			pixel_flow_y_integral /= meancount;
			offset.x = pixel_flow_x_integral;
			offset.y = pixel_flow_y_integral;
			float flow_quality = 255.0*meancount/offset_x.size();
			//printf("quality %.2f",flow_quality);
		}
	}
	
	
}
int main(int, char**)
{
    VideoCapture cap(0);
    help();
    if( !cap.isOpened() )
        return -1;

    Mat flow, cflow, frame;
    Mat frameT;
    Mat gray, prevgray, uflow;
    Point2f viewoffset;
    namedWindow("flow", 0);
	FILE *file,*filex,*filey;
	char tag = 0xfe;
	float dx,dy;
	file = fopen("log.dat","w+");;
	filex = fopen("logx.dat","w+");
	filey = fopen("logy.dat","w+");
    for(;;)
    {
        cap >> frame;
        cvtColor(frame, frameT, COLOR_BGR2GRAY);
		resize(frameT,gray,Size(320,240));
        if( !prevgray.empty() )
        {
            float logData[3] = {0.0};
            calcOpticalFlowFarneback(prevgray, gray, uflow, 0.5, 3, 15, 3, 5, 1.2, 0);
            getoffset2(uflow,viewoffset);
            printf("%.2f %.2f\n",viewoffset.x,viewoffset.y);
            dx = viewoffset.x;
            dy = viewoffset.y;
            //fwrite(&tag,sizeof(tag),1,file);
			fwrite(&dx,sizeof(dx),1,filex);
            fwrite(&dy,sizeof(dy),1,filey);
            cvtColor(prevgray, cflow, COLOR_GRAY2BGR);
            uflow.copyTo(flow);
            drawOptFlowMap(flow, cflow, 16, 1.5, Scalar(0, 255, 0));
            imshow("flow", cflow);
        }
        if(waitKey(10)>=0)
            break;
        std::swap(prevgray, gray);
    }
    fclose(file);
    fclose(filex);
    fclose(filey);
    return 0;
}

