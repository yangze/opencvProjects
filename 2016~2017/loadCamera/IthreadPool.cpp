#include "IthreadPool.h"

//static Thread_pool *pool = NULL;

IThreadPool::IThreadPool(int threadMax)
{
    if(threadMax < 0)
    {
        LOG("warning:thread MAX NUMBER invalid\n");
    }
    else
    {
        if(pool_init(threadMax) < 0)
        {
            LOG("error:init thread pool error\n");
        }
    }
}
IThreadPool::~IThreadPool()
{
    if(pool)
    {
        pool->shutdown = 1;
        pthread_cond_broadcast(&(pool->queue_ready));  //wake up all thread thread pool is being destroy
        for(int i = 0; i < pool->max_thread_num; i++)
            pthread_join(pool->threadid[i],NULL);
        //free(pool->threadid);
        delete [] pool->threadid;
        Thread_worker *head = NULL;
        while (pool->queue_head != NULL)
        {
            head = pool->queue_head;
            pool->queue_head = pool->queue_head->next;
            delete head;
            //free(head);
        }
        pthread_mutex_destroy(&(pool->queue_lock));
        pthread_cond_destroy(&(pool->queue_ready));

        //free(pool);
        delete pool;
        pool=NULL;
    }
}
int IThreadPool::pool_init(int max_thread_num)
{
    pool = new Thread_pool;
    if(!pool)
        return -1;
    pthread_mutex_init(&(pool->queue_lock), NULL); //init lock
    pthread_cond_init(&(pool->queue_ready), NULL); //ready
    pool->queue_head = NULL;                        //clear work queue
    pool->max_thread_num = max_thread_num;          //number of active process
    pool->cur_queue_size = 0;                       //clear work queue
    pool->shutdown = 0;                             //thread close flag
    pool->threadid = new pthread_t[max_thread_num];
    for(int i=0;i<max_thread_num;i++)
    {
        int ret = pthread_create(&(pool->threadid[i]),NULL,thread_routine,(void*)this);
        if(ret != 0)
        {
            LOG("cannot create thread:%s\n",strerror(ret));
            return ret;
        }
    }
}
void *IThreadPool::thread_routine(void *arg)
{
    IThreadPool *this_ = (IThreadPool *)arg;
    if(this_ == NULL)
    	pthread_exit (NULL);
    this_->run_main_routine();
    pthread_exit (NULL);  // never reach here
}
void IThreadPool::run_main_routine()
{
    //printf ("starting thread 0x%x\n", pthread_self ());
    while (1)
    {
        int ret = pthread_mutex_lock(&(pool->queue_lock));
        while(pool->cur_queue_size == 0 && !pool->shutdown)
        {
            //printf ("thread 0x%x is waiting\n", pthread_self ());
            pthread_cond_wait (&(pool->queue_ready), &(pool->queue_lock)); //cond_wait atom operation,
        }

        if(pool->shutdown)
        {
            pthread_mutex_unlock (&(pool->queue_lock));                     // unlock first
            //LOG("thread 0x%x will exit\n", pthread_self ());
            pthread_exit(NULL);
        }
        //printf ("thread 0x%x is starting to work\n", pthread_self ());
        assert (pool->cur_queue_size != 0);
        assert (pool->queue_head != NULL);

        pool->cur_queue_size--;                                         //work queue reduce 1 and delete list head member
        Thread_worker *worker = pool->queue_head;
        pool->queue_head = worker->next;
        pthread_mutex_unlock (&(pool->queue_lock));

        (*(worker->process))(worker->arg);

        delete worker;
        worker = NULL;
    }
}
int IThreadPool::pool_add_worker (void *(*process) (void *arg), void *arg)
{
    if(process == NULL)
    {
    	LOG("warning:worker routine is null\n");
    	return -1;
    }
    Thread_worker *newworker = new Thread_worker;
    newworker->process = process;                   //add process for a thread
    newworker->arg = arg;                           //arguments
    newworker->next = NULL;                         //next work set NULL

    pthread_mutex_lock (&(pool->queue_lock));
    Thread_worker *member = pool->queue_head;       //add new task to list tail
    if (member != NULL)
    {
        while (member->next != NULL)
            member = member->next;
        member->next = newworker;
    }
    else
    {
        pool->queue_head = newworker;
    }

    assert (pool->queue_head != NULL);              //assert

    pool->cur_queue_size++;
    pthread_mutex_unlock (&(pool->queue_lock));

    pthread_cond_signal(&(pool->queue_ready));      //wake up a thread who are blocked
    return 0;
}
