#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "opencv2/opencv.hpp"
#include "getKeyVal.h"
#include "takePicture.h"
using namespace cv;
using namespace std;
#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480

#define W_NUM 9
#define H_NUM 6

#define SAM_FRAMES 10


#define N_TOP		409
#define N_BOTTOM	320
#define N_LEFT		640
#define N_RIGHT		543

#define N_WIDTH 1280
#define N_HEIGHT 960
//
static bool runAndSave(const string& outputFilename,
                const vector<vector<Point2f> >& imagePoints,
                Size imageSize, Size boardSize,float squareSize,
                float aspectRatio, int flags, Mat& cameraMatrix,
                Mat& distCoeffs, bool writeExtrinsics, bool writePoints);
static bool runCalibration( vector<vector<Point2f> > imagePoints,
                    Size imageSize, Size boardSize,float squareSize, 
                    float aspectRatio,
                    int flags, Mat& cameraMatrix, Mat& distCoeffs,
                    vector<Mat>& rvecs, vector<Mat>& tvecs,
                    vector<float>& reprojErrs,
                    double& totalAvgErr);
static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners);
static double computeReprojectionErrors(
        const vector<vector<Point3f> >& objectPoints,
        const vector<vector<Point2f> >& imagePoints,
        const vector<Mat>& rvecs, const vector<Mat>& tvecs,
        const Mat& cameraMatrix, const Mat& distCoeffs,
        vector<float>& perViewErrors );
static void saveCameraParams( const string& filename,
                       Size imageSize, Size boardSize,
                       float squareSize, float aspectRatio, int flags,
                       const Mat& cameraMatrix, const Mat& distCoeffs,
                       const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                       const vector<float>& reprojErrs,
                       const vector<vector<Point2f> >& imagePoints,
                       double totalAvgErr );
pthread_t ops_thread;
char option_g = 0;
void *scan_keyboard_thread(void *p)
{
	int exit_flag = 1;//quit this process flag
	char option = 0;
    set_disp_mode(STDIN_FILENO,0);
	while(exit_flag)
    {
        while(kbhit() == 0);
        option = getchar();

        switch(option)
        {
            case 'n'://exopsure gain add
            		option_g = 'n';
            		printf("g = %c\n",option_g);
                     break;
         	case 'q':
                    exit_flag = 0;
                    option_g = 'q';
                    printf("g = %c\n",option_g);
                    set_disp_mode(STDIN_FILENO,1); // recover original attribute of terminal
                     break;
			case 'p':
                    option_g = 'p';
                     break;
        }
    }
    pthread_exit(NULL);
}
static bool runAndSave(const string& outputFilename,
                const vector<vector<Point2f> >& imagePoints,
                Size imageSize, Size boardSize,float squareSize,
                float aspectRatio, int flags, Mat& cameraMatrix,
                Mat& distCoeffs, bool writeExtrinsics, bool writePoints )
{
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(imagePoints, imageSize, boardSize, squareSize,
                   aspectRatio, flags, cameraMatrix, distCoeffs,
                   rvecs, tvecs, reprojErrs, totalAvgErr);
    printf("%s. avg reprojection error = %.2f\n",
           ok ? "Calibration succeeded" : "Calibration failed",
           totalAvgErr);

    if( ok )
        saveCameraParams( outputFilename, imageSize,
                         boardSize, squareSize, aspectRatio,
                         flags, cameraMatrix, distCoeffs,
                         writeExtrinsics ? rvecs : vector<Mat>(),
                         writeExtrinsics ? tvecs : vector<Mat>(),
                         writeExtrinsics ? reprojErrs : vector<float>(),
                         writePoints ? imagePoints : vector<vector<Point2f> >(),
                         totalAvgErr );
    return ok;
}
static bool runCalibration( vector<vector<Point2f> > imagePoints,
                    Size imageSize, Size boardSize,
                    float squareSize, float aspectRatio,
                    int flags, Mat& cameraMatrix, Mat& distCoeffs,
                    vector<Mat>& rvecs, vector<Mat>& tvecs,
                    vector<float>& reprojErrs,
                    double& totalAvgErr)
{
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        cameraMatrix.at<double>(0,0) = aspectRatio;
	printf("in runCalibration\n");
    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcChessboardCorners(boardSize, squareSize, objectPoints[0]);

    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    double rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,
                    distCoeffs, rvecs, tvecs, flags|CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);
                    ///*|CV_CALIB_FIX_K3*/|CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);
    printf("RMS error reported by calibrateCamera: %g\n", rms);

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

    return ok;
}
static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners )
{
    corners.resize(0);

    for( int i = 0; i < boardSize.height; i++ )
        for( int j = 0; j < boardSize.width; j++ )
            corners.push_back(Point3f(float(j*squareSize),float(i*squareSize), 0));
}
static double computeReprojectionErrors(
        const vector<vector<Point3f> >& objectPoints,
        const vector<vector<Point2f> >& imagePoints,
        const vector<Mat>& rvecs, const vector<Mat>& tvecs,
        const Mat& cameraMatrix, const Mat& distCoeffs,
        vector<float>& perViewErrors )
{
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); i++ )
    {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float)std::sqrt(err*err/n);
        totalErr += err*err;
        totalPoints += n;
    }

    return std::sqrt(totalErr/totalPoints);
}
static void saveCameraParams( const string& filename,
                       Size imageSize, Size boardSize,
                       float squareSize, float aspectRatio, int flags,
                       const Mat& cameraMatrix, const Mat& distCoeffs,
                       const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                       const vector<float>& reprojErrs,
                       const vector<vector<Point2f> >& imagePoints,
                       double totalAvgErr )
{
    FileStorage fs( filename, FileStorage::WRITE );

    time_t tt;
    time( &tt );
    struct tm *t2 = localtime( &tt );
    char buf[1024];
    strftime( buf, sizeof(buf)-1, "%c", t2 );

    fs << "calibration_time" << buf;

    if( !rvecs.empty() || !reprojErrs.empty() )
        fs << "nframes" << (int)std::max(rvecs.size(), reprojErrs.size());
    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    fs << "board_width" << boardSize.width;
    fs << "board_height" << boardSize.height;
    fs << "square_size" << squareSize;

    if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        fs << "aspectRatio" << aspectRatio;

    if( flags != 0 )
    {
        sprintf( buf, "flags: %s%s%s%s",
            flags & CV_CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
            flags & CV_CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
            flags & CV_CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
            flags & CV_CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "" );
        cvWriteComment( *fs, buf, 0 );
    }

    fs << "flags" << flags;

    fs << "camera_matrix" << cameraMatrix;
    fs << "distortion_coefficients" << distCoeffs;

    fs << "avg_reprojection_error" << totalAvgErr;
    if( !reprojErrs.empty() )
        fs << "per_view_reprojection_errors" << Mat(reprojErrs);

    if( !rvecs.empty() && !tvecs.empty() )
    {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for( int i = 0; i < (int)rvecs.size(); i++ )
        {
            Mat r = bigmat(Range(i, i+1), Range(0,3));
            Mat t = bigmat(Range(i, i+1), Range(3,6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "extrinsic_parameters" << bigmat;
    }

    if( !imagePoints.empty() )
    {
        Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
        for( int i = 0; i < (int)imagePoints.size(); i++ )
        {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "image_points" << imagePtMat;
    }
}
int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	{
		fs ["camera_matrix"] >> cameraMatrix;
		fs ["distortion_coefficients"] >> distCoeffs;
		printf("load end\n");
		return 0;
	}
	return -1;
}

int calibrator(const char * path,Mat &view)
{
	if(!path)
		return -1;
	printf("%s\n",path);
	vector<string> imageList;
	static bool bLoadCameraParams = false;
	static Mat cameraMatrix,distCoeffs,map1,map2;
	Mat rview;
	Size imageSize,newImageSize;
	
	if(!view.data)
		return -1;
	imageSize.width = view.cols;
	imageSize.height = view.rows;
	
	newImageSize.width = imageSize.width;
	newImageSize.height = imageSize.height;
	loadCameraParams(path,cameraMatrix,distCoeffs);
	
	initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),  
    getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, newImageSize, 0), newImageSize, CV_16SC2, map1, map2);
    
	remap(view, rview, map1, map2, INTER_LINEAR);  

	//imshow("Image View", rview);
	//waitKey(0);
	//int c = 0xff & waitKey();  

	rview.copyTo(view); 
	return 0;
}

static const char *outputFilename = "cameraParam.xml";
void help()
{
	printf("run eg: ./camera 0\n");
	printf("cmd 0->calibrate 1->undistortion\n");
}
int main(int argc,char **argv)
{
	
	if(argc <= 1)
	{
		printf("input parameter error\n");
		help();
		return -1;
	}
	
	//cmd temp
	int cmd = atoi(argv[1]);
	int camID = atoi(argv[2]);
	VideoCapture cap(camID);
	Size imageSize;
	Size newImageSize;
	vector<vector<Point2f> > imagePoints;
	vector<Point2f> cornersBuf;
	bool cornersFindFlag = false;
	Mat frame;
	Mat gray;
	Mat grayTmp;
	Mat map1,map2;
	Mat rview;
	Mat rviewROI;
	float squareSize = 0.25f, aspectRatio = 1.f;
	bool writeExtrinsics = true, writePoints = true;
	Mat cameraMatrix, distCoeffs;
	int flags = 0;
	bool calib_flag = false;
	
	imageSize.width = IMAGE_WIDTH;
	imageSize.height = IMAGE_HEIGHT;
	newImageSize.width = N_WIDTH;
	newImageSize.height = N_HEIGHT;
	
	if(cmd)
		calib_flag = true;
	if(argv[3] && calib_flag)
	{
		int ret = loadCameraParams(argv[3],cameraMatrix, distCoeffs);
		if(ret < 0)
		{
			printf("load %s error\n",argv[3]);
			return -1;
		}
		//
		initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),  
						getOptimalNewCameraMatrix(cameraMatrix,distCoeffs, imageSize, 1, newImageSize, 0), newImageSize, CV_16SC2, map1, map2);
		cout<<"new Image size"<<newImageSize<<endl;
	}

	if(!cap.isOpened())
	{
		printf("can not open camera\n");
		return -1;
	}
	printf("open camera success\n");
	// init parameters
	cap.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	cap.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);

	// create show window
	namedWindow("ori",WINDOW_NORMAL);
	namedWindow("undi",WINDOW_NORMAL);
	resizeWindow("ori",IMAGE_WIDTH,IMAGE_HEIGHT);
	resizeWindow("undi",newImageSize.width,newImageSize.height);
	pthread_create(&ops_thread,NULL,scan_keyboard_thread,NULL);
	//loop
	while(1)
	{
		vector<Point2f> corners;
		bool found;
		cap >> frame;
		if(!frame.data)
			break;
		cvtColor(frame,gray,CV_BGR2GRAY);
		grayTmp = gray;
		if(!calib_flag)
			found = findChessboardCorners(gray,Size(W_NUM,H_NUM),corners,CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE+CALIB_CB_FAST_CHECK);
		else
			found = false;
		if(found && option_g == 'n' && !calib_flag)
		{
			option_g  = 0;
			cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
			drawChessboardCorners(gray,cvSize(W_NUM,H_NUM), Mat(corners), found);
			imagePoints.push_back(corners);
			if(imagePoints.size() >= SAM_FRAMES && !calib_flag)
			{
				calib_flag = true;
				printf("get 10 frames image ok \n");
				//run calibration process and save camera params
				if( runAndSave(outputFilename, imagePoints, imageSize,
                       Size(W_NUM,H_NUM), squareSize, aspectRatio,
                       flags, cameraMatrix, distCoeffs,
                       writeExtrinsics, writePoints))
				{
					printf("calibration success\n");
				}	
			}
			printf("current saved frame %d\n",(int)imagePoints.size());
			printf("input n for next capture process\n");

		}
		else if(found && !calib_flag)
		{
			cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
			drawChessboardCorners(gray,cvSize(W_NUM,H_NUM), Mat(corners), found);
		}
		if(calib_flag)//undistortion
		{
			remap(gray, rview, map1, map2, INTER_LINEAR);
			rviewROI = rview(Rect(N_TOP,N_BOTTOM,N_LEFT,N_RIGHT));
		}
		if(option_g == 'q')
		{
			/*vector<int> compression_params;
			compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
			compression_params.push_back(9);
			imwrite("sampleImag.png",rview,compression_params);*/
			pthread_join(ops_thread,NULL);
			cout<<"new size"<<rview.size()<<endl;
			break;
		}
		if(option_g == 'p')
		{
			option_g = 0;
			char fileName[20] = {0};
			int index = 0;
			
			if( (index = getPicNum()) >= 0 )
			{
				sprintf(fileName,"eulerPicture%d.png",index);
				vector<int> compression_params;
				compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
				compression_params.push_back(9);
				imwrite(fileName,frame,compression_params);
			}
		}
		imshow("ori",grayTmp);
		imshow("undi",rviewROI);
		if(waitKey(30) >= 0)
		{
		
		}
	}
	return 0;
}
