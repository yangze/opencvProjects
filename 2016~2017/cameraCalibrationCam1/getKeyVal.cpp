#include "getKeyVal.h"
//for get keyvalue
#define ECHOFLAGS (ECHO | ECHOE | ECHOK | ECHONL)

/*************************************************
Function name: kbhit
Parameter    : void
Description	 : 猫聨路氓聫聳茅聰庐莽聸聵猫鸥聯氓聟楼
Return		 : int茂艗聦忙聹聣猫鸥聯氓聟楼猫驴聰氓聸聻1茂艗聦氓聬艩氓聢聶猫驴聰氓聸聻0
Argument     : void
Autor & date :
**************************************************/
extern "C"
{
	int kbhit(void)
	{
		struct termios oldt, newt;
		int ch;
		int oldf;
		tcgetattr(STDIN_FILENO, &oldt);
		newt = oldt;
		newt.c_lflag &= ~(ICANON | ECHO);
		tcsetattr(STDIN_FILENO, TCSANOW, &newt);
		oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
		fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
		ch = getchar();
		tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
		fcntl(STDIN_FILENO, F_SETFL, oldf);
		if(ch != EOF)
		{
		        ungetc(ch, stdin);
		        return 1;
		}
		return 0;
	}

	int set_disp_mode(int fd,int option)
	{
	   int err;
	   struct termios term;
	   if(tcgetattr(fd,&term)==-1){
		 perror("Cannot get the attribution of the terminal");
		 return 1;
	   }
	   if(option)
		    term.c_lflag|=ECHOFLAGS;
	   else
		    term.c_lflag &=~ECHOFLAGS;
	   err=tcsetattr(fd,TCSAFLUSH,&term);
	   if(err==-1 && err==EINTR){
		    perror("Cannot set the attribution of the terminal");
		    return 1;
	   }
	   return 0;
	}
}
