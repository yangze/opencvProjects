//
//    demo - demonstration code to show the usage of AGAST, an adaptive and
//           generic corner detector based on the accelerated segment test
//
//    Copyright (C) 2010  Elmar Mair
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cvaux.h>
#include <highgui.h>

// include C headers
extern "C"
{
	#include "agast5_8.h"
	#include "agast7_12s.h"
	#include "agast7_12d.h"
	#include "oast9_16.h"
}

using namespace std;

//threshold for accelerated segment test (16-, 12- and 8-pixel mask)
#define AST_THR_16	40
#define AST_THR_12	38
#define AST_THR_8	27

enum AST_PATTERN {OAST9_16, AGAST7_12d, AGAST7_12s, AGAST5_8, AST_PATTERN_LENGTH};


static void drawResult(IplImage const * const imageGray, IplImage * const imageOut, int numCorners_all, CvPoint const * const corners_all, int numCorners_nms, CvPoint const * const corners_nms)
{
	cvCvtColor( imageGray, imageOut, CV_GRAY2RGB );
	for(int i=0; i < numCorners_all; i++ )
	{
		cvLine( imageOut, cvPoint( corners_all[i].x, corners_all[i].y ), cvPoint( corners_all[i].x, corners_all[i].y ), CV_RGB(255,0,0) );
	}
	for(int i=0; i < numCorners_nms; i++ )
	{
		//points
		cvLine( imageOut, cvPoint( corners_nms[i].x, corners_nms[i].y ), cvPoint( corners_nms[i].x, corners_nms[i].y ), CV_RGB(0,255,0) );
		//crosses
//		cvLine( imageOut, cvPoint( corners_nms[i].x-1, corners_nms[i].y ),	cvPoint( corners_nms[i].x+1, corners_nms[i].y ), CV_RGB(0,255,0) );
//		cvLine( imageOut,	cvPoint( corners_nms[i].x, corners_nms[i].y-1 ), cvPoint( corners_nms[i].x, corners_nms[i].y+1 ), CV_RGB(0,255,0) );
	}
}


int main(int argc, char* argv[])
{
	char *name_imageIn;
	CvPoint *corners_all, *corners_nms;
	int numCorners_all, numCorners_nms;
	IplImage *imageIn, *imageGray, *imageOut;
	int cols, rows;

	//check program parameters
	if( argc !=2 ) {
		printf( "Wrong number of arguments - need 1 argument:\ndemo <image_in.xxx>\ne.g. demo demo.ppm\n" );
		exit(0);
	}
	name_imageIn = argv[1];

	cout << "Starting demo...\n";

	//load image and convert it to 8 bit grayscale
	imageIn = cvLoadImage( name_imageIn, -1 );
	if(!imageIn)
	{
		cout << "Image \"" << name_imageIn << "\" could not be loaded.\n";
	    exit(0);
	}
	imageGray = cvCreateImage( cvGetSize( imageIn ), IPL_DEPTH_8U, 1);
	cvCvtColor( imageIn, imageGray, CV_RGB2GRAY );

	cols = imageGray->width;
	rows = imageGray->height;

	imageOut=cvCreateImage( cvGetSize( imageIn ), IPL_DEPTH_8U, 3);

	for(int j=0; j<AST_PATTERN_LENGTH; j++)
	{
		cvCvtColor( imageGray, imageOut, CV_GRAY2RGB );
		switch(j)
		{
			case OAST9_16:
				cout << "OAST9_16: ";
				corners_all=oast9_16((unsigned char *)imageGray->imageData, cols, rows, AST_THR_16, &numCorners_all);
				corners_nms=oast9_16_nms((unsigned char *)imageGray->imageData, cols, AST_THR_16, numCorners_all, corners_all, &numCorners_nms);
				cout << numCorners_all << " corner responses - " << numCorners_nms << " corners after non-maximum suppression." << endl;
				drawResult(imageGray, imageOut, numCorners_all, corners_all, numCorners_nms, corners_nms);
				cvSaveImage( "oast9_16.ppm", imageOut );
				break;
			case AGAST7_12d:
				cout << "AGAST7_12d: ";
				corners_all=agast7_12d((unsigned char *)imageGray->imageData, cols, rows, AST_THR_12, &numCorners_all);
				corners_nms=agast7_12d_nms((unsigned char *)imageGray->imageData, cols, AST_THR_12, numCorners_all, corners_all, &numCorners_nms);
				cout << numCorners_all << " corner responses - " << numCorners_nms << " corners after non-maximum suppression." << endl;
				drawResult(imageGray, imageOut, numCorners_all, corners_all, numCorners_nms, corners_nms);
				cvSaveImage( "agast7_12d.ppm", imageOut );
				break;
			case AGAST7_12s:
				cout << "AGAST7_12s: ";
				corners_all=agast7_12s((unsigned char *)imageGray->imageData, cols, rows, AST_THR_12, &numCorners_all);
				corners_nms=agast7_12s_nms((unsigned char *)imageGray->imageData, cols, AST_THR_12, numCorners_all, corners_all, &numCorners_nms);
				cout << numCorners_all << " corner responses - " << numCorners_nms << " corners after non-maximum suppression." << endl;
				drawResult(imageGray, imageOut, numCorners_all, corners_all, numCorners_nms, corners_nms);
				cvSaveImage( "agast7_12s.ppm", imageOut );
				break;
			case AGAST5_8:
				cout << "AGAST5_8: ";
				corners_all=agast5_8((unsigned char *)imageGray->imageData, cols, rows, AST_THR_8, &numCorners_all);
				corners_nms=agast5_8_nms((unsigned char *)imageGray->imageData, cols, AST_THR_8, numCorners_all, corners_all, &numCorners_nms);
				cout << numCorners_all << " corner responses - " << numCorners_nms << " corners after non-maximum suppression." << endl;
				drawResult(imageGray, imageOut, numCorners_all, corners_all, numCorners_nms, corners_nms);
				cvSaveImage( "agast5_8.ppm", imageOut );
				break;
			default:
				break;
		}
	}

	//free memory
	void agast9_16_freeMemory();
	void agast7_12d_freeMemory();
	void agast7_12s_freeMemory();
	void agast5_8_freeMemory();
	cvReleaseImage(&imageIn);
	cvReleaseImage(&imageGray);
	cvReleaseImage(&imageOut);

	cout << "...done!\n";

	return 0;
}
