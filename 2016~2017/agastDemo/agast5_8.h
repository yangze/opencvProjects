//
//    agast5 - AGAST, an adaptive and generic corner detector based on the
//              accelerated segment test for a 8 pixel mask
//
//    Copyright (C) 2010  Elmar Mair
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AGAST5_8_H
#define AGAST5_8_H

struct CvPoint;

struct CvPoint* agast5_8(const unsigned char* im, int xsize, int ysize, int b, int* num_corners);
struct CvPoint* agast5_8_nms(const unsigned char* im, int xsize, int b, int numCorners_all, struct CvPoint const * const corners_all, int * const numCorners_nms);
void agast5_8_freeMemory();

#endif /* AGAST5_8_H */
