#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <cctype>
#include <stdio.h>
#include <string.h>
#include <time.h>

using namespace cv;
int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	
	fs ["camera_matrix"] >> cameraMatrix;
	fs ["distortion_coefficients"] >> distCoeffs;
}

int calibrator(const char * path,Mat &view)
{
	if(!path)
		return -1;
	printf("%s\n",path);
	vector<string> imageList;
	static bool bLoadCameraParams = false;
	static Mat cameraMatrix,distCoeffs,map1,map2;
	Mat rview;
	Size imageSize,newImageSize;
	
	if(!view.data)
		return -1;
	imageSize.width = view.cols;
	imageSize.height = view.rows;
	
	newImageSize.width = imageSize.width;
	newImageSize.height = imageSize.height;
	loadCameraParams(path,cameraMatrix,distCoeffs);
	
	initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),  
    getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, newImageSize, 0), newImageSize, CV_16SC2, map1, map2);
    
	remap(view, rview, map1, map2, INTER_LINEAR);  

	//imshow("Image View", rview);
	//waitKey(0);
	//int c = 0xff & waitKey();  

	rview.copyTo(view); 
	return 0;
}
int main(int argc,char **argv)
{
	if(argc != 3)
	{
		printf("input parameter error\n");
		printf("input cmd eg: ./undistortion xxx.jpg camera.yml\n");
	}
	namedWindow("undis",WINDOW_NORMAL);
	namedWindow("ori",WINDOW_NORMAL);
	Mat img = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
	Mat ori = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
	calibrator(argv[2],img);
	imshow("undis",img);
	imshow("ori",ori);
	waitKey(0);
	return 0;	
}
