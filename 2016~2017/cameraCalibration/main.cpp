#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>

#include "Itime.h"

using namespace cv;
using namespace std;

#define W_NUM 15
#define H_NUM 10
ITimer timer;
int main(int argc,char **argv)
{
	int64_t start,end;
	Mat left,right;
	vector<Point2f> corners_l;
	vector<Point2f> corners_r;
	right = imread("fish1.jpeg",CV_LOAD_IMAGE_GRAYSCALE);
	left =  imread("fish1.jpeg",CV_LOAD_IMAGE_GRAYSCALE);
	namedWindow("left",WINDOW_NORMAL);
	namedWindow("right",WINDOW_NORMAL);
	
	//find chessboard corners
	bool patternfound_l = findChessboardCorners(left,cvSize(W_NUM,H_NUM),corners_l,CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
+ CALIB_CB_FAST_CHECK);
	bool patternfound_r = findChessboardCorners(right,cvSize(W_NUM,H_NUM),corners_r,CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
+ CALIB_CB_FAST_CHECK);
	if(patternfound_l)
	{
		printf("find success in pic %s\n","left01.jpg");
		cornerSubPix(left, corners_l, Size(11, 11), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
	}
	if(patternfound_r)
	{
		printf("find success in pic %s\n","right01.jpg");
		cornerSubPix(right, corners_r, Size(11, 11), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
	}
	drawChessboardCorners(left, cvSize(W_NUM,H_NUM), Mat(corners_l), patternfound_l);
	drawChessboardCorners(right, cvSize(W_NUM,H_NUM), Mat(corners_r), patternfound_r);
	imshow("left",left);
	imshow("right",right);
	waitKey(0);
	return 0;
} 
