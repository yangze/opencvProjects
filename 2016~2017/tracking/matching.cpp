#include "matching.h"
#include <iostream>
int matcingProcess(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<vector<int> > &haar_like_val1,vector<vector<int> > &haar_like_val2)
{
	if(!gray1.data || !gray2.data)
	{
		return -1;
	}
	//
	double qualityLevel = 0.01;
	double minDistance = 30;
	int blockSize = 5;
	bool useHarrisDetector=true;
	double k=0.04;
	goodFeaturesToTrack(gray1,corners1,COTNERSNUM,qualityLevel,minDistance,Mat(),blockSize,useHarrisDetector,k);
	goodFeaturesToTrack(gray2,corners2,COTNERSNUM,qualityLevel,minDistance,Mat(),blockSize,useHarrisDetector,k);
	
	/*getHaarLikeValue(gray1,corners1,haar_like_val1);
	getHaarLikeValue(gray2,corners2,haar_like_val2);*/
	vector<vector<int> > sadResult;
	vector<int> sadMin;
	vector<int> cornersIndex;
    /*getSAD1Value(gray1,gray2,corners1,corners2,sadResult,SQUARE);
    matchSADValue(sadResult,sadMin,corners1.size());
    getCornersIndex(sadResult,sadMin,cornersIndex,corners1.size());
    drawOffset(gray2,corners1,corners2,cornersIndex);
    //show sad min
    printf("**************************\n");
    for(int i=0;i<sadMin.size();i++)
    {
    	printf("%d\n",sadMin[i]);
    }
    //show index
    printf("**************************\n");
    for(int i=0;i<cornersIndex.size();i++)
    {
    	printf("%d\n",cornersIndex[i]);
    }*/
}
int drawOffset(Mat &gray,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<int> &cornersIndex)
{
	int sameCornerNumber = cornersIndex.size();
	int beforeFrameCorners =corners1.size() ;
	int CurrentFrameCorners = corners2.size();
	for(int i=0;i<beforeFrameCorners;i++) //draw before corners
		circle(gray,corners1[i],CIRCLER,Scalar(0,0,255));
	for(int i=0;i<CurrentFrameCorners;i++)//draw curent corners
		circle(gray,corners2[i],CIRCLER,Scalar(0,0,255));
	for(int i=0;i<sameCornerNumber;i++)
		line(gray,corners2[i],corners1[cornersIndex[i]],Scalar(0,0,255));
}
int getCornersIndex(vector<vector<int> > &sadResult,vector<int> &sadMin,vector<int> &cornersIndex,int cols)
{
	int m = sadResult.size();
	int n = cols;
	if(sadResult.size()>0 && sadMin.size()>0)
	{
		for(int i=0;i<m;i++)
		{
			int index = 0;
			for(int j=0;j<n;j++)
			{
				if(sadResult[i][j] == sadMin[i])
				{
					index = j;
					break;
				}
			}
			cornersIndex.push_back(index);
		}
		return 0;
	}
	return -1;
}
int matchSADValue(vector<vector<int> > &sadResult,vector<int> &sadMin,int cols)
{
	int m = sadResult.size();
	int n = cols;
	
	if(sadResult.size() > 0)
	{
		for(int i=0;i<m;i++)
		{
			int tempMin = sadResult[i][0];
			for(int j=1;j<n;j++)
			{
				if(sadResult[i][j] < tempMin)
					tempMin = sadResult[i][j];
			}
			sadMin.push_back(tempMin);
		}
		//printf("size %d \n",sadResult[i].size());
		return 0;
	}
	return -1;
}
int getSAD1Value(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<vector<int> > &sadResult,int SADPattern)
{
    int SADSquareSize = 7;
    int m = corners2.size();
    int n = corners1.size();
	if(corners1.size() > 0 && corners2.size() >0)
    {
        switch(SADPattern)
        {
        case SQUARE:
            for(int i=0;i<m;i++)
            {
                sadResult.push_back(vector<int>(n));
                for(int j=0;j<n;j++)
                {
                    int SADTemp = ISAD_1(gray1,gray2,corners1[i],corners2[j],SADSquareSize);
                    if(SADTemp < 0)
                    	sadResult[i][j] = SADSquareSize*SADSquareSize*255;
                	else
                		sadResult[i][j] = SADTemp;
                    //printf("(%d,%d)-(%d,%d) =%d \n",corners1[i].y,corners1[i].x,corners2[j].y,corners2[j].x,sadResult[i][j]);
                    printf("%d ",sadResult[i][j]);
                }
                printf("\n");
            }
            break;
        case RECTANGLE:

            break;
        case CIRCLE:

            break;
        default:
            printf("unknow SAD pattern mode\n");
        }
        return 0;
    }
    return -1;
}
int getHaarLikeValue(Mat &gray,vector<Point2i> &corners,vector<vector<int> > &haar_like_val)
{
	if(corners.size() > 0)
	{
		for(int i=0;i<corners.size();i++)
		{
			haar_like_val.push_back(vector<int>(HAARLIKENUM));
			int haarLike1 = HaarLike_1(gray,corners[i],7);
			int haarLike2 = HaarLike_2(gray,corners[i],7);
			int haarLike3 = HaarLike_3(gray,corners[i],9,5);
			int haarLike4 = HaarLike_4(gray,corners[i],5,9);
			int haarLike5 = HaarLike_5(gray,corners[i],7);
			haar_like_val[i][0] = haarLike1;
			haar_like_val[i][1] = haarLike2;
			haar_like_val[i][2] = haarLike3;
			haar_like_val[i][3] = haarLike4;
			haar_like_val[i][4] = haarLike5;
		}
	}
	return 0;
}
int drawCorners(Mat &frame,vector<Point2i> &points,Scalar color)
{
	if(!frame.data || points.size() == 0 || points.size() < 0)
		return -1;
	for(int i=0;i<points.size();i++)
		circle(frame,points[i],CIRCLER,color);
}
/*
*typical usage:HaarLike_1(gray1,Point2i(5,42),7);
*suqareSize must be odd number typical value 7 9 ...
*Pattern a (edge features)
*/
int HaarLike_1(Mat &gray,Point2i center,int suqareSize)
{
	int numT = suqareSize/2;
	int width = gray.cols;
	int height = gray.rows;
	int sumBlack = 0;
	int sumWhite = 0;
	if(center.x-numT<0 || center.x+numT>(width-1) || center.y-numT<0 || center.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		return -1;
	}
	for(int i=center.y-numT;i<center.y+numT+1;i++)//black area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x+1;j<(center.x+numT+1);j++)
		{
			sumBlack += ptr[j];
		}
	}
	for(int i=center.y-numT;i<center.y+numT+1;i++)//white area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x-numT;j<center.x;j++)
		{
			sumWhite += ptr[j];
		}
	}
	return sumWhite-sumBlack;
}
/*
*typical usage:HaarLike_2(gray1,Point2i(5,42),7);
*suqareSize must be odd number typical value 7 9 ...
*Pattern b(edge feature)
*/
int HaarLike_2(Mat &gray,Point2i center,int suqareSize)
{
	int numT = suqareSize/2;
	int width = gray.cols;
	int height = gray.rows;
	int sumBlack = 0;
	int sumWhite = 0;
	if(suqareSize%2 == 0)
	{
		printf("square size must be odd\n");
		return -1;
	}
	if(center.x-numT<0 || center.x+numT>(width-1) || center.y-numT<0 || center.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		return -1;
	}
	for(int i=center.y+1;i<center.y+numT+1;i++)//black area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x-numT;j<(center.x+numT+1);j++)
		{
			sumBlack += ptr[j];
		}
	}
	for(int i=center.y-numT;i<center.y;i++)//white area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x-numT;j<(center.x+numT+1);j++)
		{
			sumWhite += ptr[j];
		}
	}
	return sumWhite-sumBlack;
}
/*
*typical usage:HaarLike_3(gray1,Point2i(5,42),7,3);
*window width is bigger than height w>h
* w--h typical value(7,3) (9,5) (11,7)...
*Pattern a(line features)
*/
int HaarLike_3(Mat &gray,Point2i center,int w,int h)
{
	int numW = w/2;
	int numH = h/2;
	int width = gray.cols;
	int height = gray.rows;
	int sumBlack = 0;
	int sumWhite = 0;
	if(w%2==0 || h%2==0)
	{
		printf("square size must be odd\n");
		return -1;
	}
	if(center.x-numW<0 || center.x+numW>(width-1) || center.y-numH<0 || center.y+numH>(height-1))
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		return -1;
	}
	for(int i=center.y-numH;i<center.y+numH+1;i++)//black area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j =center.x-numH;j<(center.x+numH+1);j++)
		{
			sumBlack += ptr[j];
		}
	}
	for(int i=center.y-numH;i<center.y+numH+1;i++)//white area 1
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x-numW;j<(center.x-numH);j++)
		{
			sumWhite += ptr[j];
		}
	}
	for(int i=center.y-numH;i<center.y+numH+1;i++)//white area 2
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=center.x+numH+1;j<(center.x+numW+1);j++)
		{
			sumWhite += ptr[j];
		}
	}
	return sumWhite-sumBlack;
}
/*
*typical usage:HaarLike_4(gray1,Point2i(5,42),3,7);
*window height bigger than width h>w
* w--h typical value(3,7) (5,9) (7,11)...
*Pattern c(line features)
*/
int HaarLike_4(Mat &gray,Point2i center,int w,int h)
{
	int numW = w/2;
	int numH = h/2;
	int width = gray.cols;
	int height = gray.rows;
	int sumBlack = 0;
	int sumWhite = 0;
	if(w%2==0 || h%2==0)
	{
		printf("w and h must be odd\n");
		return -1;
	}
	if(center.x-numH<0 || center.x+numH>(height-1) || center.y-numW<0 || center.y+numW>(width-1))
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		return -1;
	}
	for(int i=center.x-numW;i<center.x+numW+1;i++)//black area
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j =center.y-numW;j<(center.y+numW+1);j++)
		{
			sumBlack += ptr[j];
		}
	}
	for(int i=center.x-numH;i<center.x-numW;i++)//white area 1
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j =center.y-numW;j<(center.y+numW+1);j++)
		{
			sumWhite += ptr[j];
		}
	}
	for(int i=center.x+numW+1;i<center.x+numH+1;i++)//white area 2
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j =center.y-numW;j<(center.y+numW+1);j++)
		{
			sumWhite += ptr[j];
		}
	}
	return sumWhite-sumBlack;
}
/*
*typical usage:HaarLike_5(gray1,Point2i(5,42),5);
*suqareSize must be odd : typical value 5 7 9... bigger than 3
*Pattern a(center circle features)
*/
int HaarLike_5(Mat &gray,Point2i center,int suqareSize)
{
	int centerSquare = suqareSize-2;
	int numT = suqareSize/2;
	int numC = centerSquare/2;
	int width = gray.cols;
	int height = gray.rows;
	int sumBlack = 0;
	int sumWhite = 0;
	if(center.x-numT<0 || center.x+numT>(height-1) || center.y-numT<0 || center.y+numT>(width-1))
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		return -1;
	}
	if(suqareSize%2 == 0)
	{
		printf("suqareSize must be odd\n");
		return -1;
	}
	if(centerSquare == 1)//center square size is 1
	{
		for(int i=center.x-numT;i<center.x+numT+1;i++)
		{
			uint8_t *ptr = gray.ptr<uint8_t>(i);
			for(int j=center.y-numT;j<center.y+numT+1;j++)
			{
				sumWhite+=ptr[j];
			}
		}
		uint8_t *ptr = gray.ptr<uint8_t>(center.x);
		sumBlack = ptr[center.y];
		sumWhite -= sumBlack;
	}
	else				//center squre size >1 typical size 3 5 7 9...
	{
		for(int i=center.x-numT;i<center.x+numT+1;i++)//sauqre all pixel sum
		{
			uint8_t *ptr = gray.ptr<uint8_t>(i);
			for(int j=center.y-numT;j<center.y+numT+1;j++)
			{
				sumWhite+=ptr[j];
			}
		}
		for(int i=center.x-numC;i<center.x+numC+1;i++)//black area pixel sum
		{
			uint8_t *ptr = gray.ptr<uint8_t>(i);
			for(int j=center.y-numC;j<center.y+numC+1;j++)
			{
				sumBlack+=ptr[j];
			}
		}
		sumWhite -= sumBlack;
	}
	return sumWhite-sumBlack;
}
int ISAD_1(Mat &gray1,Mat &gray2,Point2i center1,Point2i center2,int suqareSize)
{
	int numT = suqareSize/2;
	int width = gray1.cols;
	int height = gray1.rows;
	int SAD = 0;
	int coor_x_1 = 0;
	int coor_y_1 = 0;
	int coor_x_2 = 0;
	int coor_y_2 = 0;
	if(suqareSize%2 == 0)
	{
		printf("square size must be odd\n");
		return -1;
	}
	if(center1.x-numT<0 || center1.x+numT>(width-1) || center1.y-numT<0 || center1.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		printf("center1 (%d,%d)\n",center1.x,center1.y);
		return -1;
	}
	if(center2.x-numT<0 || center2.x+numT>(width-1) || center2.y-numT<0 || center2.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		printf("center2 (%d,%d)",center2.x,center2.y);
		return -1;
	}
	coor_x_1 = center1.x - numT;
	coor_y_1 = center1.y - numT;
	
	coor_x_2 = center2.x - numT;
	coor_y_2 = center2.y - numT;
	//printf("center %d %d\n",center1.x,center1.y);
	for(int i=0;i<suqareSize;i++)
	{
		uint8_t *ptr1 = gray1.ptr<uint8_t>(coor_y_1+i);
		uint8_t *ptr2 = gray2.ptr<uint8_t>(coor_y_2+i);
		for(int j=0;j<suqareSize;j++)
		{
			SAD+=abs(ptr1[coor_x_1+j]-ptr2[coor_x_2+j]);
		}
	}
	return SAD;
}
