#ifndef __TAKEPICTURE_H
#define __TAKEPICTURE_H
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "opencv2/opencv.hpp"
using namespace cv;
int getPicNum();
int writeMatToFile(char *fileName,Mat &gray);
#endif
