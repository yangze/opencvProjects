#!/bin/bash

#MODE_CAMERA = 0,
#MODE_PIC = 1,
#MODE_TRACKING = 2,

make


case "$1" in
"cam")
	./tracking 0
	;;
"img")	
	./tracking 1
	;;
"track")
	./tracking 2
	;;
"")
	echo ""
	echo "input cmd error"
	echo ""
	;;
esac

# param1:0 camera calibration 1 camera undistortion
# param2:0 -> camera 1,   1 -> camera 2
# eg: run calibration
#./camera 0 0

#eg: run undistortion
#./camera 1 0 cameraParam.xml
./recover
