#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include "opencv2/opencv.hpp"
#include "getKeyVal.h"
#include "takePicture.h"
#include "Itime.h"
#include "matching.h"
#include "matching2.h"
#define TIME_START(x) gettimeofday(&(x),NULL);
#define TIME_END(x) gettimeofday(&(x),NULL);
#define TIME_USE(start,end) 1000000*((end).tv_sec-(start).tv_sec)+((end).tv_usec-(start).tv_usec);

using namespace cv;
using namespace std;
#define IMAGE_WIDTH 320
#define IMAGE_HEIGHT 240

#define W_NUM 9
#define H_NUM 6

#define SAM_FRAMES 10


#define N_TOP		409
#define N_BOTTOM	320
#define N_LEFT		640
#define N_RIGHT		543

#define N_WIDTH 1280
#define N_HEIGHT 960
//
#define MAXCORNERS 10
#define CAM_L_ID 1
#define CAM_R_ID 0

#define WIN_WIDTH 640
#define WIN_HEIGHT 480
enum CMDSET
{
	MODE_CAMERA = 0,
	MODE_PIC = 1,
	MODE_TRACKING = 2,
};
pthread_t ops_thread;

static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners );

char option_g = 0;
void *scan_keyboard_thread(void *p)
{
	int exit_flag = 1;//quit this process flag
	char option = 0;
    set_disp_mode(STDIN_FILENO,0);
	while(exit_flag)
    {
        while(kbhit() == 0);
        option = getchar();

        switch(option)
        {
            case 'n'://exopsure gain add
            		option_g = 'n';
                     break;
         	case 'q':
                    exit_flag = 0;
                    option_g = 'q';
                    set_disp_mode(STDIN_FILENO,1); // recover original attribute of terminal
                     break;
         	case 'r':
         			option_g = 'r';
         			break;
            case 's':
                    option_g = 's';
                     break; 
			case 'p':
                    option_g = 'p';
                     break;
        }
    }
    pthread_exit(NULL);
}

int loadCameraParams(const char * path,Mat &cameraMatrix,Mat &distCoeffs)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	{
		fs ["camera_matrix"] >> cameraMatrix;
		fs ["distortion_coefficients"] >> distCoeffs;
		return 0;
	}
	return -1;
}
int loadStereoParams(const char * path,Mat &R,Mat &T,Mat &Rl,Mat &Rr,Mat &Pl,Mat &Pr,Mat &Q)
{
	if(!path)
		return -1;
	FileStorage fs(path,FileStorage::READ);
	if(fs.isOpened())
	{
		fs["R"] >> R;
		fs["T"] >> T;
		fs["Rl"] >> Rl;
		fs["Rr"] >> Rr;
		fs["Pl"] >> Pl;
		fs["Pr"] >> Pr;
		fs["Q"] >> Q;
		return 0;
	}
	return -1;
}
static void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners )
{
    corners.resize(0);

    for( int i = 0; i < boardSize.height; i++ )
        for( int j = 0; j < boardSize.width; j++ )
            corners.push_back(Point3f(float(j*squareSize),float(i*squareSize), 0));
}
int runCalibration(vector<vector<Point2f> >imagePointsL,vector<vector<Point2f> >imagePointsR,
					Mat cameraMatrixL,Mat distCoeffsL,Mat cameraMatrixR,Mat distCoeffsR,
					Size imageSize,Size boardSize,Mat& R,Mat& T,Mat& E,Mat& F)
{
	const float squareSize = 26.5f;  // Set this to your actual square size
	vector<vector<Point3f> > objectPoints(1);

	calcChessboardCorners(boardSize, squareSize, objectPoints[0]);
	objectPoints.resize(imagePointsL.size(),objectPoints[0]);
	
	stereoCalibrate(objectPoints,imagePointsL,imagePointsR,cameraMatrixL,distCoeffsL,
					cameraMatrixR,distCoeffsR,imageSize,R,T,E,F,
					cvTermCriteria(CV_TERMCRIT_ITER+
        CV_TERMCRIT_EPS, 100, 1e-5));
	return 0;
}
static const char *cameraLParameters = "cameraParamL.xml";
static const char *cameraRParameters = "cameraParamR.xml";
char currentDir[512] = "0";
void help()
{
	printf("run eg:./steroCamera 0 cameraParamL.xml cameraParamR.xml\n");
	printf("cmd 0...\n");
}

int main(int argc,char **argv)
{
	
	if(argc < 2)
	{
		printf("input parameter error\n");
		help();
		return -1;
	}
	
	//cmd temp
	int cmd = atoi(argv[1]);
	
	VideoCapture capture(0);
	Mat frame;
	Mat frameCurrent;
	Mat gray;
	Mat grayCurrent;
	Size imageSize;
	ITimer timer;
	vector<Point2i> cornersT;
	vector<Point2i> cornersTCurrent;
	vector<Point2i> forcastPosiCam;
	struct timeval tpstart,tpend;
	float timeuse = 0.0;
	int64_t start,end;
	//goodfeatureToTrace parameters
	double qualityLevel = 0.01;
	double minDistance = 30;
	int blockSize = 5;
	bool useHarrisDetector=true;
	double k=0.04;
	imageSize.width = IMAGE_WIDTH;
	imageSize.height = IMAGE_HEIGHT;
	
	printf("cmd %d\n",cmd);
	if(cmd == MODE_TRACKING || cmd == MODE_CAMERA)
	{
		if(!capture.isOpened())
		{
			printf("can not open camera\n");
			cmd = 0;
		}
	}
	getcwd(currentDir,sizeof(currentDir));
	printf("current dir %s\n",currentDir);
	capture.set( CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
	capture.set( CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
	
	namedWindow("camera",WINDOW_NORMAL);
	resizeWindow("camera",WIN_WIDTH,WIN_HEIGHT);
	
	pthread_create(&ops_thread,NULL,scan_keyboard_thread,NULL);// key value get process
	int frameIndex = 0;
	while(1)
	{
		if(cmd == MODE_TRACKING) //camera mode
		{
			//if(option_g == 'n')
			{
				if(frameIndex == 0)//store as past frame
				{
					frameIndex = 1;
					capture >> frame;
					if(!frame.data)
					{
						break;
					}
					cvtColor(frame,gray,CV_BGR2GRAY);
					waitKey(30);
				}
				else if(frameIndex == 1)//store as current frame
				{
					frameIndex = 0;
					capture >> frameCurrent;
					if(!frameCurrent.data)
					{
						break;
					}
					cvtColor(frameCurrent,grayCurrent,CV_BGR2GRAY);
					matcingProcess2(gray,grayCurrent,cornersT,cornersTCurrent,forcastPosiCam);//
					drawCorners(frameCurrent,cornersTCurrent,Scalar(0,0,255));//R
					drawCorners(frameCurrent,forcastPosiCam,Scalar(255,0,0));//B
					drawOffsetLine(frameCurrent,cornersT,forcastPosiCam);
					forcastPosiCam.clear();
					imshow("camera",frameCurrent);
					waitKey(30);
				}
				
			}
			if(option_g == 's')//save all calibrated parameters
			{
				option_g = 0;
				//save parameters here
			}
			if(option_g == 'r')//recalibration process
			{
				option_g = 0;
		
			}

			if(option_g == 'p')
			{
				option_g = 0;
				char fileName[20] = {0};
				int index = 0;
		
				if( (index = getPicNum()) >= 0 )
				{
					sprintf(fileName,"eulerPicture%d.png",index);
					vector<int> compression_params;
					compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
					compression_params.push_back(9);
					imwrite(fileName,frame,compression_params);
				}
			}
		}
		else if(cmd == MODE_CAMERA)//image mode
		{
			capture >> frame;
			if(!frame.data)
			{
				printf("get invalid frame data\n");
				break;
			}
			cvtColor(frame,gray,CV_BGR2GRAY);
			imshow("camera",frame);
			waitKey(33);
			if(option_g == 'p')
			{
				option_g = 0;
				getcwd(currentDir,sizeof(currentDir));
				char fileName[512] = {0};
				int index = 0;
		
				if( (index = getPicNum()) >= 0 )
				{
					sprintf(fileName,"eulerPicture%d.png",index);
					vector<int> compression_params;
					compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
					compression_params.push_back(9);
					imwrite(fileName,frame,compression_params);
				}
			}
		}
		else if(cmd == MODE_PIC)
		{
			Mat img1 = imread("eulerPicture3.png",CV_LOAD_IMAGE_COLOR);
			Mat img2 = imread("eulerPicture4.png",CV_LOAD_IMAGE_COLOR);
			Mat grayT1;
			Mat grayT2;
			Mat binaryImg1;
			Mat binaryImg2;
			vector<Point2i> corners1;
			vector<Point2i> corners2;
			vector<vector<int> > haar_like_val1;
			vector<vector<int> > haar_like_val2;
			vector<Point2i> forcastPosi;
			cvtColor(img1,grayT1,CV_RGB2GRAY);
			cvtColor(img2,grayT2,CV_RGB2GRAY);
			
			if(!grayT1.data || !grayT2.data)
			{
				printf("load images error\n");
				return -1;
			}
			/*adaptiveThreshold(grayT1,binaryImg1,255,ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY,3,5);
			adaptiveThreshold(grayT2,binaryImg2,255,ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY,3,5);*/
			// matching process
			//writeMatToFile("image2.txt",grayT2);
			//matcingProcess(grayT1,grayT2,corners1,corners2,haar_like_val1,haar_like_val2);
			matcingProcess2(grayT1,grayT2,corners1,corners2,forcastPosi);
			
			//drawCorners(grayT2,forcastPosi,Scalar(0,0,255));//
			//drawCorners(grayT2,corners2,Scalar(0,0,255));//
			
			drawCorners(img2,corners1,Scalar(0,0,255));//R
			//drawCorners(img2,corners2,Scalar(0,255,0));//G
			drawCorners(img2,forcastPosi,Scalar(255,0,0));//B
			drawCorners(img1,corners1,Scalar(0,0,255));//after blue
			drawOffsetLine(img2,corners1,forcastPosi);
			/*for(int i=0;i<haar_like_val1.size();i++)
			{
				printf("%d %d %d %d %d \n",haar_like_val1[i][0],haar_like_val1[i][1],haar_like_val1[i][2],haar_like_val1[i][3],haar_like_val1[i][4]);
				//printf("%d\n",haar_like_val1[i][0]);
			}
			printf("*********************************\n");
			for(int i=0;i<haar_like_val2.size();i++)
			{
				printf("%d %d %d %d %d \n",haar_like_val2[i][0],haar_like_val2[i][1],haar_like_val2[i][2],haar_like_val2[i][3],haar_like_val2[i][4]);
				//printf("%d\n",haar_like_val2[i][0]);
			}*/
			//drawCorners(grayT1,corners1,Scalar(0,0,255));//before red
			//drawCorners(img2,corners2,Scalar(255,0,0));//after blue
			//drawCorners(img2,corners1,Scalar(0,0,255));//after blue
			namedWindow("pic1",WINDOW_NORMAL);
			namedWindow("pic2",WINDOW_NORMAL);
			resizeWindow("pic1",WIN_WIDTH,WIN_HEIGHT);
			resizeWindow("pic2",WIN_WIDTH,WIN_HEIGHT);
			while(1)
			{
				imshow("pic1",img1);
				imshow("pic2",img2);
				waitKey(30000);
			}
		}
		if(option_g == 'q')//end process
		{
			pthread_join(ops_thread,NULL);
			printf("process exit \n");
			break;
		}
	}
	return 0;
}
