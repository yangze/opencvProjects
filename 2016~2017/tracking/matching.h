#ifndef _MATCHING_H
#define _MATCHING_H
#include <stdio.h>
#include "opencv2/opencv.hpp"
using namespace cv;

#define COTNERSNUM 20
#define CIRCLER 4
#define HAARLIKENUM 5
enum sadPattern
{
    SQUARE = 0,
    RECTANGLE = 1,
    CIRCLE = 2,
};
int matcingProcess(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<vector<int> > &haar_like_val1,vector<vector<int> > &haar_like_val2);
int drawCorners(Mat &frame,vector<Point2i> &points,Scalar color);

int HaarLike_1(Mat &gray,Point2i center,int suqareSize);
int HaarLike_2(Mat &gray,Point2i center,int suqareSize);
int HaarLike_3(Mat &gray,Point2i center,int w,int h);
int HaarLike_4(Mat &gray,Point2i center,int w,int h);
int HaarLike_5(Mat &gray,Point2i center,int suqareSize);

int ISAD_1(Mat &gray1,Mat &gray2,Point2i center1,Point2i center2,int suqareSize);


int getHaarLikeValue(Mat &gray,vector<Point2i> &corners,vector<vector<int> > &haar_like_val);
int getSAD1Value(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<vector<int> > &sadResult,int SADPattern);
int matchSADValue(vector<vector<int> > &sadResult,vector<int> &sadMin,int cols);
int getCornersIndex(vector<vector<int> > &sadResult,vector<int> &sadMin,vector<int> &cornersIndex,int cols);

int drawOffset(Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<int> &cornersIndex);//before red

#endif
