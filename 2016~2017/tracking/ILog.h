#ifndef _ILOG_H
#define _ILOG_H

#include <stdio.h>
#include <errno.h>
#include <string.h>

//debug macro LOG : print to stand out
#define DEBUGEXE

#ifdef DEBUGEXE
#define LOG(format,...) printf(format,##__VA_ARGS__)
#else
#define LOG(format,...)
#endif // DEBUGEXE

#endif // _ILOG_H
