#ifndef _MATCHING2_H
#define _MATCHING2_H

#include <stdio.h>
#include "opencv2/opencv.hpp"
using namespace cv;

#define COTNERSNUM 20
#define CIRCLER 4
#define HAARLIKENUM 5

typedef struct sadCoor
{
	int SAD;
	int row;
	int col;
}SADAndPoint2i;

int matcingProcess2(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<Point2i> &forcastPosi);

int forecastPosition(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &forcastPosi,int area);
int getSAD(Mat &gray1,Mat &gray2,Point2i coor1,Point2i coor2,int squareSize);

int drawOffsetLine(Mat &gray,vector<Point2i> &points1,vector<Point2i> &points2);
#endif
