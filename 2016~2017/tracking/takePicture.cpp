#include "takePicture.h"
#include "opencv2/opencv.hpp"
char tmp[] = "111111111111111111111111111111111111111111";

int getPicNum()
{
	int index = 0;
	
	system("ls *.png > imageList.txt");
	//
	FILE *file = NULL;
	file = fopen("imageList.txt","r");
	if(!file)
	{
		printf("opencv file imageList.txt error\n");
		return -1;
	}
	// count image file numbers
	while(!feof(file))
	{
		fgets(tmp,sizeof(tmp),file);
		index++;
	}
	printf("index = %d\n",index);
	return index;
}
int writeMatToFile(char *fileName,Mat &gray)
{
	if(!gray.data || !fileName )
		return -1;
	FILE *file;
	int m=gray.rows;
	int n=gray.cols;
	file = fopen(fileName,"w+");
	if(!file)
		return -1;
	for(int i=0;i<m;i++)
	{
		uint8_t *ptr = gray.ptr<uint8_t>(i);
		for(int j=0;j<n;j++)
		{
			fprintf(file,"%d ",ptr[j]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
	return 0;
}
