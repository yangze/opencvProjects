#include "matching2.h"
#include "matching.h"
#include <iostream>
int matcingProcess2(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &corners2,vector<Point2i> &forcastPosi)
{
	int SearchAreaR = 7;
	if(!gray1.data || !gray2.data)
	{
		return -1;
	}
	//
	double qualityLevel = 0.01;
	double minDistance = 30;
	int blockSize = 5;
	bool useHarrisDetector=true;
	double k=0.04;
	goodFeaturesToTrack(gray1,corners1,COTNERSNUM,qualityLevel,minDistance,Mat(),blockSize,useHarrisDetector,k);
	goodFeaturesToTrack(gray2,corners2,COTNERSNUM,qualityLevel,minDistance,Mat(),blockSize,useHarrisDetector,k);
	forecastPosition(gray1,gray2,corners1,forcastPosi,SearchAreaR);
	//forecastPosition(gray1,gray2,corners1,forcastPosi,area);
}

int forecastPosition(Mat &gray1,Mat &gray2,vector<Point2i> &corners1,vector<Point2i> &forcastPosi,int searchArea)
{
	int searchBasePointsNumber = corners1.size();
	int searchNewWindowSize = searchArea*2+1;
	int sadWinSize = 5;
	if(corners1.size() > 0)
	{
		for(int i=0;i<corners1.size();i++)
		{
			int startRows = corners1[i].y-searchArea;
			int startCols = corners1[i].x-searchArea;
			vector<SADAndPoint2i> sadVTemp;
			for(int m = 0;m < searchNewWindowSize;m++)
			{
				for(int n = 0;n < searchNewWindowSize;n++)
				{
					SADAndPoint2i sadPoint2i;
					int sadTemp = getSAD(gray1,gray2,corners1[i],Point2i(startCols+n,startRows+m),sadWinSize);
					if(sadTemp < 0)
					{
						sadPoint2i.SAD = sadWinSize*sadWinSize*255;
						sadPoint2i.row=startRows+m;
						sadPoint2i.col=startCols+n;
						sadVTemp.push_back(sadPoint2i);
					}
					else
					{
						sadPoint2i.SAD = sadTemp;
						sadPoint2i.row=startRows+m;
						sadPoint2i.col=startCols+n;
						//sadPoint2i.coor=Point2i(startCols+n,startRows+m);
						sadVTemp.push_back(sadPoint2i);
					}
					//printf("(%d,%d) %d\n", corners1[i].y, corners1[i].x,sadTemp);
				}
			}
			//get min sad value
			int sadSizeTemp = sadVTemp.size();
			int sadMinTemp =sadVTemp[0].SAD;
			for(int i =1;i<sadSizeTemp;i++)
			{
				if(sadVTemp[i].SAD < sadMinTemp)
				{
					sadMinTemp = sadVTemp[i].SAD;
				}
			}
			for(int i =0;i<sadSizeTemp;i++)
			{
				if(sadVTemp[i].SAD == sadMinTemp)
				{
					//printf("min %d  %d (%d,%d)******\n",sadMinTemp,i,sadVTemp[i].row,sadVTemp[i].col);
					forcastPosi.push_back(Point2i(sadVTemp[i].col,sadVTemp[i].row));
					break;
				}
			}
		}
		//printf("size %d\n",forcastPosi.size());
		return 0;
	}
	return -1;
}

int getSAD(Mat &gray1,Mat &gray2,Point2i center1,Point2i center2,int suqareSize)
{
	int numT = suqareSize/2;
	int width = gray1.cols;
	int height = gray1.rows;
	int SAD = 0;
	int coor_x_1 = 0;
	int coor_y_1 = 0;
	int coor_x_2 = 0;
	int coor_y_2 = 0;
	if(suqareSize%2 == 0)
	{
		printf("square size must be odd\n");
		return -1;
	}
	if(center1.x-numT<0 || center1.x+numT>(width-1) || center1.y-numT<0 || center1.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		//printf("center1 (%d,%d)\n",center1.x,center1.y);
		return -1;
	}
	if(center2.x-numT<0 || center2.x+numT>(width-1) || center2.y-numT<0 || center2.y+numT>(height-1) )
	{
#ifdef IDEBUG
		printf("invalid operation\n");
#endif
		//printf("center2 (%d,%d)",center2.x,center2.y);
		return -1;
	}
	coor_x_1 = center1.x - numT;
	coor_y_1 = center1.y - numT;
	coor_x_2 = center2.x - numT;
	coor_y_2 = center2.y - numT;
	for(int i=0;i<suqareSize;i++)
	{
		uint8_t *ptr1 = gray1.ptr<uint8_t>(coor_y_1+i);
		uint8_t *ptr2 = gray2.ptr<uint8_t>(coor_y_2+i);
		for(int j=0;j<suqareSize;j++)
		{
			SAD+=abs(ptr1[coor_x_1+j]-ptr2[coor_x_2+j]);
			//printf("%d ",ptr2[coor_x_2+j]);
		}
		//printf("\n");
	}
	//printf("**********\n");
	return SAD;
}

int drawOffsetLine(Mat &gray,vector<Point2i> &points1,vector<Point2i> &points2)
{
	int sameCornerNumber = points1.size();
	for(int i=0;i<sameCornerNumber;i++)
		line(gray,points1[i],points2[i],Scalar(0,255,255));
}
